# SVG Out Cloud add-in
The SVG Out Cloud add-in is intended to bring broader SVG functionality to Atlassian Confluence Cloud. Confluence Cloud natively supports the output of most SVGs. As the SVGs are embedded as image your will loose the functionality of embedded links however. This add-in comes with some additional features like crop, pan&zoom and link replacement.

## License
Please refer to our [Source code license agreement](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/15826959/Source+code+license+agreement)

## Manual
Please refer to the Wiki pages of this repository.

## Branches
The sources contain two branches. As the "dev" branch is work in progress you should only use the sources of the master branch.

## Other dependencies
SVG Out uses an external service app to convert SVGs to PNGs when the user is working browser which does not support the svg->canvas->dataURL approach. This app is based on phantom.js but is not public due to copyright issues of the fonts I bundle with the app. In case you need to have the sources let me know.

## Building

### Building for development
When you want to run the application in "development" mode you have to

1. Comment out "spring.profiles.active=production" in application.properties
2. Adopt the baseUrl in atlassian-connect.json to the URL your add-on is reachable 
2. Run the following command

mvn spring-boot:run -P dev


### Building for Heroku
The sources are ready to be deployed to Heroku as they are. Of course you have to adopt the baseUrl in atlassian-connect.json to the URL of your dyno.


## Change log

### 2.11.5

Infrastructure:

* Upgrade commons.io

### 2.11.4

Infrastructure:

* Upgrade to Spring Boot 3.4.3
* Upgrade to ACSB 5.1.7

### 2.11.3

Infrastructure:

* Upgrade to Spring Boot 3.4.1
* Upgrade to ACSB 5.1.5

### 2.11.2

Infrastructure:

* Upgrade DOMPurify to 3.2.1

### 2.11.1

Infrastructure:

* Upgrade to Spring Boot 3.2.11
* Upgrade to ACSB 5.1.4

### 2.11.0

Infrastructure:

* Upgrade to Spring Boot 3.2.9
* Upgrade to ACSB 5.1.3
* Support export in case of custom baseUrls
* Fix error thrown in case of migrated SVGs during export

### 2.10.10

Infrastructure:

* Upgrade to Spring Boot 3.2.6
* Upgrade to ACSB 4.1.2
* Upgrade DOMPurify to 3.1.5

### 2.10.9

Infrastructure:

* Upgrade to Spring Boot 3.2.5

### 2.10.8

Infrastructure:

* Upgrade to Spring Boot 3.2.3
* Upgrade to ACSB 4.1.1
* Upgrade other dependencies

### 2.10.7

Infrastructure:

* Upgrade to Spring Boot 3.1.6
* Upgrade to ACSB 4.0.7

### 2.10.6

Infrastructure:

* Upgrade to Spring Boot 3.1.5
* Upgrade to ACSB 4.0.6
* Upgrade to DOMPurify 3.0.6
* Switch to Confluence REST API v2

### 2.10.5

Infrastructure:

* Upgrade to Spring Boot 3.1.4
* Upgrade to ACSB 4.0.5
* Upgrade to JAVA 17
* Upgrade to json 20231013 

### 2.10.4

Infrastructure:

* Upgrade to Spring Boot 2.7.16
* Upgrade to ACSB 3.0.7
* Upgrade to DOMPurify 3.0.5
* Remove minify plugin


### 2.10.3

Bug fix:

* Re-enable theming with proper no-theme detection


### 2.10.2

Bug fix:

* Disable theming again due to UI issues on some systems


### 2.10.1

Infrastructure:

* Upgrade to Spring Boot 2.7.14
* Override Thymeleaf version to 3.1.2


### 2.10.0

Improvement:

* Add support for Confluence's Dark mode
* Upgrade to SB 2.7.13


### 2.9.4

Improvement:

* Switch to site specific hashes for settings


### 2.9.3

Bug fixes:

* Export PNGs with absolute URL (Atlassian obviously changed something)


### 2.9.2

Infrastructure:

* Upgrade to Spring Boot 2.7.12
* Upgrade to ACSB 3.0.4
* Upgrade json to 20230227
* Upgrade to DOMPurify 3.0.3
* Include full paths in export


### 2.9.1

Infrastructure:

* Upgrade to Spring Boot 2.7.10
* Remove googleapis
* Upgrade to DOMPurify 3.0.1
* Switch to Java 17


### 2.9.0

Infrastructure:

* Enforce the use of 2.8.3 and above


### 2.8.3

Infrastructure:

* Upgrade to DOM Purify 3.0.0
* Add v2above as parameter


### 2.8.2

Infrastructure:

* Upgrade to Spring Boot 2.7.8
* Upgrade to ACSB 3.0.2
* Set Content-Disposition for external SVGs


### 2.8.1

Infrastructure:

* Upgrade to Spring Boot 2.7.6
* Upgrade postgresql


### 2.8.0

Features:

* Add an option to bypass the strict content security policy


### 2.7.0

Features:

* Add DOMPurify as alternative security mode

Infrastructure:

* Upgrade to Spring Boot 2.7.3
* Upgrade to Atlassian Connect Spring Boot 2.3.6
* Comply with newest Cloud Security guidelines


### 2.6.9

Infrastructure:

* Upgrade to Spring Boot 2.6.8


### 2.6.8

Infrastructure:

* Upgrade to Atlassian Connect Spring Boot 2.3.4


### 2.6.7

Infrastructure:

* Upgrade to Atlassian Connect Spring Boot 2.3.3
* Upgrade to Spring Boot 2.6.6


### 2.6.6

Infrastructure:

* Upgrade to Atlassian Connect Spring Boot 2.3.2
* Upgrade to Spring Boot 2.6.5


### 2.6.5

Infrastructure:

* Upgrade to Atlassian Connect Spring Boot 2.3.1
* Upgrade to Spring Boot 2.6.4
* Upgrade other dependencies


### 2.6.4

Infrastructure:

* Upgrade to Spring Boot 2.5.10
 

### 2.6.3

Infrastructure:

* Override Tomcat version to avoid duplicate accept headers as suggested on https://github.com/spring-projects/spring-framework/issues/26434#issuecomment-1047599788 


### 2.6.2

Infrastructure:

* Upgrade to Spring Boot 2.5.9
* Upgrade postgresql driver to avoid CVE-2022-21724


### 2.6.1

Infrastructure:

* Remove migration code again
* Get and set settings as current user not as app


### 2.6.0

Bug fixes:

* Fix issue related to SVGs without width

Infrastructure:

* Avoid undetected manipulation of settings by unauthorized users


### 2.5.5

Infrastructure:

* Update Spring Boot to 2.5.8


### 2.5.4

Infrastructure:

* Update libs


### 2.5.3

Bug fixes:

* Fix issue related to viewboxes with commas


### 2.5.2

Bug fixes:

* Atlassian Connect installation lifecycle security improvements


### 2.5.1

Bug fixes:

* Context QSH related fix as required by Atlassian


### 2.5.0

Features / improvements:

* Improve permanent link highlighting


### 2.4.0

Features / improvements:

* Change arrangement of parameters for a better usability


### 2.3.0

Features / improvements:

* Allow selection of a raster resolution

Bug fixes:

* Fix alignment in exports


### 2.2.1

Bug fixes:

* Remove web hooks from atlassian-connect.json


### 2.2.0

Features / improvements:

* Switch to SHA instead of MD5


### 2.1.0

Features / improvements:

* Improve element selection when defining links


### 2.0.0

Features / improvements:

* Align user interface styles to the current Confluence look and feel
* Improve migration from Server or Data Center
* Make calls on behalf of the current user not the app - this reduces permission management efforts
* Add a second level of security checks
* Add options to highlight links permanently and on hover

Bug fixes:

* Fix alignment issue (left, center, right)

Infrastructure:

* Cleanup code
* Upgrade to Spring Boot 2.4.0
* Upgrade to Atlassian Connect Spring Boot 2.1.2
* Remove Internet Explorer specific code


### 1.2.7

Bugs:

* Solve XSS vulnerabilities


### 1.2.6

Bugs:

* Solve XSS vulnerabilities


### 1.2.5

Bugs:

* Solve XSS vulnerability
* Solve a bug related to zooming with the mouse wheel under Chrome


### 1.2.4

Bugs:

* Solve inline width rendering issue
 

### 1.2.3

Infrastructure:

* Upgrade to Atlassian Connect Sprint Boot 2.0.1
* Upgrade to Spring Boot Version 2.1.7
* Take all.js from connect-cdn.atl-paas.net


### 1.2.2

Infrastructure:

* Send GDPR true
* Upgrade to Atlassian Connect Sprint Boot 1.5.1


### 1.2.1

Improvments:

* Support inline output also in export


### 1.2.0

Improvements:

* Add inline version of the macro

Infrastructure

* Upgrade to Atlassian Connect Spring Boot 1.5.0


### 1.1.14

Infrastructure

* Enforce license

### 1.1.13

Infrastructure

* Add license reminder

### 1.1.12

Infrastructure

* Enable licensing (paid via Atlassian)

### 1.1.11

Bugs

* Solve issue #8


### 1.1.10

Infrastructure

* Change button bind to comply with Atlassian Connect JavaScript V5


### 1.1.9

Bugs

* Close security related bug #7


### 1.1.8

Infrastructure

* Upgrade to JavaScript V5
* Upgrade to Atlassian AUI 6.0.7


### 1.1.7

Infrastructure

* Update to Atlassian Connect Spring Boot version 1.3.5
* Update to Spring Boot version 1.5.3


### 1.1.6

Bugs

* Add target information to all links (issue #6)


### 1.1.5

Features/improvements

* Remove some overhead needed for fixing issue #5 temporary
* Make width of selection frame independent from zoom level 

Infrastructure

* Change to atlassian connect spring boot version 1.3.1
* Change access to all.js


### 1.1.4

Bugs

* Solve issue #5 finally

Infrastructure

* Update postgresql driver


### 1.1.3

Bugs

* Temporary workaround for issue #5

Infrastructure

* Update cache only when a SVG was been updated or trashed


### 1.1.1 and 1.1.2

Atlassian triggered version change - no differences to 1.1.0


### 1.1.0

Features/improvements

* Implement PNG generation for PDF and Word export (#1)
* Allow adding of links not just replacing existing links

Bugs

* Close issue #3

Infrastructure

* Move to spring-boot version 1.4.2
* Move to atlassian-connect-spring-boot version 1.2.0
* Change callback authorization from iframe to plugin


### 1.0.1

Bugs

* Render SVGs with transparent background not white


### 1.0.0

initial release


