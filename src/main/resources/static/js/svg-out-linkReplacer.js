function replaceLinks(baseUrl, addReplace) {

	// the xlink name space; we need this for appending the node
	var  xlinkns = "http://www.w3.org/1999/xlink";
	
	// our SVG
	var svg = $('.svg-out').find('svg')[0];
		
	// give all SVG an id in case they don't have one
	var elements = $(svg).find('*');
	for(var n = 0; n < elements.length; n ++) {
		if(!elements[n].hasAttribute('id')) {
			elements[n].setAttribute('id', elements[n].tagName + '-svg-out-' + n.toString());
		}
	}
	
	if(addReplace) {
		
		// parse link list
		var linkListJSON = document.getElementsByClassName("svg-out")[0].getAttribute("linkList");
		var linkList = JSON.parse(linkListJSON);
		
		// get all links in the SVG
		var linksInSVG = svg.getElementsByTagName("a");

		// loop through all entries in the list
		for(var l = 0; l < linkList.length; l++) {
			var link = linkList[l];
			var linkInSVG = null;
			
			// are we adding a link?
			if(link.linkToReplace.indexOf("id:") == 0) {
				var element = document.getElementById(link.linkToReplace.replace("id:", ""));
				if(element) {
					element.removeAttribute("pointer-events");
					linkInSVG = document.createElementNS("http://www.w3.org/2000/svg", "a");
					element.parentNode.replaceChild(linkInSVG, element);
					linkInSVG.appendChild(element);
					populateLink(baseUrl, linkInSVG, link);
				}
			}
			
			// or are we replacing a link (legacy)
			else {
				// loop through all links in the SVG
				for(var a = 0; a < linksInSVG.length; a++) {
					linkInSVG = linksInSVG[a];
					
					// is it the link we want to replace?
					if(linkInSVG.hasAttributeNS(xlinkns, "href")) {
						if(linkInSVG.getAttributeNS(xlinkns, "href") == link.linkToReplace) {
							populateLink(baseUrl, linkInSVG, link);
						}
					}
				}
			}
		}
		
		// loop through all links in the SVG again and add a target if none is present
		for(var a = 0; a < linksInSVG.length; a++) {
			var linkInSVG = linksInSVG[a];
			
			// is it the link we want to replace?
			if(!linkInSVG.hasAttribute("target")) {
				linkInSVG.setAttribute("target", "_parent");
			}
		}		
	}
	
	// add styles for SVG link highlighting
	var css = "";
	if($(".svg-out:first").attr("linkHighlight") === "true") {
		css += ".svg-out a:hover {opacity:1.0;} .svg-out a {opacity:0.5;}";
	}
	else if($(".svg-out:first").attr("linkHighlightHover") === "true") {
		css += ".svg-out a:hover {opacity:0.5;}";
	}
	var style= document.createElementNS('http://www.w3.org/2000/svg', 'style');
	style.type = 'text/css';
	style.appendChild(document.createTextNode(css));
	svg.appendChild(style);
}


// populate the link as we need it
function populateLink(baseUrl, linkInSVG, link) {
	
	// the xlink name space; we need this for appending the node
	var  xlinkns = "http://www.w3.org/1999/xlink";

	// the additional anchor (if defined)
	var anchorAdd = "";
	if(link.anchor.length > 0) {
		anchorAdd = "#" + link.anchor;
	}
	
	// Confluence link
	if(link.type <= 1) {
		if(link.linkConf.indexOf("att") == -1) {
			linkInSVG.setAttributeNS(xlinkns, "href", baseUrl + "/pages/viewpage.action?pageId=" + link.linkConf + anchorAdd);
		}
		else {
			linkInSVG.setAttributeNS(xlinkns, "href", "#");
			setAttachmentLink(linkInSVG, baseUrl, link.linkConf);
		}
	}
	
	// external link
	else {
		linkInSVG.setAttributeNS(xlinkns, "href", sanitizeUrl(link.linkExt + anchorAdd));
	}
	
	// open in new window/tab
	if((link.type == 1) || (link.type == 3)) {
		linkInSVG.setAttribute("target", "_blank");
	}
	
	// in the same window (however outside the iframe)
	else {
		linkInSVG.setAttribute("target", "_parent");
	}
	
	// tooltip
	linkInSVG.setAttributeNS(xlinkns, "title", link.tooltip);
}


// follow a attachment link
function setAttachmentLink(link, baseUrl, attachmentID) {
	
	// the xlink name space; we need this for appending the node
	var  xlinkns = "http://www.w3.org/1999/xlink";
	
	// request attachment details and update link once received
	AP.request({
		url: '/api/v2/attachments/' + attachmentID,
		type: 'GET',
		success: function(responseJSON){
			var response = JSON.parse(responseJSON);
			link.setAttributeNS(xlinkns, "href", baseUrl + response._links.webui);
		}
	});
}


// https://github.com/braintree/sanitize-url
function isRelativeUrlWithoutProtocol(url) {
	const relativeFirstCharacters = [".", "/"];
	return relativeFirstCharacters.indexOf(url[0]) > -1;
}


// https://github.com/braintree/sanitize-url
function sanitizeUrl(url) {
	const invalidProtocolRegex = /^(%20|\s)*(javascript|data)/im;
	const ctrlCharactersRegex = /[^\x20-\x7EÀ-ž]/gim;
	const urlSchemeRegex = /^([^:]+):/gm;

	if (!url) {
		return "about:blank";
	}

	const sanitizedUrl = url.replace(ctrlCharactersRegex, "").trim();

	if (isRelativeUrlWithoutProtocol(sanitizedUrl)) {
		return sanitizedUrl;
	}

	const urlSchemeParseResults = sanitizedUrl.match(urlSchemeRegex);

	if (!urlSchemeParseResults) {
		return sanitizedUrl;
	}

	const urlScheme = urlSchemeParseResults[0];

	if (invalidProtocolRegex.test(urlScheme)) {
		return "about:blank";
	}

	return sanitizedUrl;
}