$('#saveInProgress').hide();

$("#save").click(function() {
	 saveValues(); 
});

// set the property containing our settings
function setProperty(settings) {	
	AP.context.getToken(function(token){
		$.ajax({
			url: "/settings",
			type: "PUT",
			contentType: 'application/json',
			data: JSON.stringify(settings),
			beforeSend: function (request) {
			    request.setRequestHeader("Authorization", "JWT " + token);
			},
			success: function(responseText) {
				$('#saveInProgress').hide();
			}
		});
	});
}
 
 
// get the property containing our settings
function getProperty(callback) {
	AP.context.getToken(function(token){
		$.ajax({
			url: "/settings",
			type: "GET",
			beforeSend: function (request) {
			    request.setRequestHeader("Authorization", "JWT " + token);
			},
			success: function(responseText) {
				var response = JSON.parse(responseText);
				callback(response.value);
			},
			error: function() {
				callback(null);
			}
		});
	});
}
 

// react on the save button click		 
function saveValues() {
 	$('#saveInProgress').show();
 	
 	var settings = {
 		securityMode : $('#securityMode').val(),
 		whiteList : $('#whiteList').val(),
 		exportPNGDefault : $('#exportPNGDefault').prop('checked') ? "true" : "false"
 	};
 	
	setProperty(settings);
}
 
 
// init the GUI
function initGUI(settings) {
	$('#securityMode').val(settings && settings.securityMode || "internal")
	$('#whiteList').val(settings && settings.whiteList || "");
 	$('#exportPNGDefault').prop("checked", ((settings && settings.exportPNGDefault || "false") == "true"));
}
 

// get the settings and call back init GUI when done 
getProperty(initGUI);