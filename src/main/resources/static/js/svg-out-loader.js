function svgLoader() {

	// init variables
	var svgS = "";
	var securityMode = "dompurify";
	var whiteList = "";
	
	// public function to insert to and insert the SVG
	this.loadAndInsertSVG = function(callBackWhenDone) {
		
		// clear the SVG
		svgS = "";
		
		// perform two parallel requests - one to get the SVG, the other one to get the settings
		$.when(
			loadSVG(),
			loadSettings()
		).then(function() {
			if(svgS.length > 0) {
				callBackWhenDone(checkAndInsertSVG());
			}
			else {
				callBackWhenDone("could_not_load");
			}
		});
	};
	
	
	// get the SVG as string
	this.getSVG = function() {
		return svgS;
	};
	
	
	// load the SVG
	var loadSVG = function() {
		var dfd = $.Deferred();
		
		var attachmentID = $(".svg-out:first").attr("attachmentID") || "";
		var _page = $(".svg-out:first").attr("_page") || "";
		var _name = $(".svg-out:first").attr("_name") || "";
		var svgUrl = $(".svg-out:first").attr("svgUrl") || "";
		var pageId = $(".svg-out:first").attr("pageId") || "";
		
		if(attachmentID.length > 0) {
			AP.request({
				url: '/api/v2/attachments/' + attachmentID,
				success: function(resultText) {
					var result = JSON.parse(resultText);
					if(result._links.download.toLowerCase().indexOf('svgz') == -1) {
						console.log("Loading SVG from Confluence directly");
						loadFromConfluence(result._links.download, dfd);
					}
					else {
						console.log("Loading compressed SVGZ via the app");
						loadDecompressed(result._links.download, dfd);
					}
				}
			});
		}
		else if(svgUrl.length > 0) {
			console.log("Loading external SVG(Z) via the app");
			loadExternal(svgUrl, dfd);
		}
		else if(_name.length > 0) {
			console.log("Handling migrated SVG Out macro");
			AP.request({
				url: '/rest/api/content/search?expand=container&cql=title="' + _name + '" and type=attachment',
				success: function(resultText) {
					var result = JSON.parse(resultText);
					var download = findCorrectAttachment(result.results, _page, pageId);
					if(download.length > 0 && download.indexOf('svgz') == -1) {
						loadFromConfluence(download, dfd);
					}
					else if(download.length > 0 && download.indexOf('svgz') > -1) {
						loadDecompressed(download, dfd);
					}
					else {
						console.log("Don't know what to load");
						dfd.resolve();
					}
				}
			});
		}
		else {
			console.log("Don't know what to load");
			dfd.resolve();
		}
		
		return dfd.promise();
	};
	
	
	// load image form Confluence instance
	var loadFromConfluence = function(url, dfd) {
		AP.request({
			url: url,
			type: 'GET',
			success: function(response) {
				svgS = response;
				dfd.resolve();
			},
			error: function (request, status, error) {
				dfd.resolve();
			}
		});
	};
	
	
	// load image and decompress it via the app
	var loadDecompressed = function(url, dfd) {
		AP.context.getToken(function(token){
			$.ajax({
				url: "/decompress?download=" + url,
				type: 'GET',
				beforeSend: function (request) {
				    request.setRequestHeader("Authorization", "JWT " + token);
				},
				success: function(response) {
					svgS = response;
					dfd.resolve();
				},
				error: function (request, status, error) {
					dfd.resolve();
				}
			});
		});
	};
	
	
	// load external image via the app
	var loadExternal = function(url, dfd) {
		AP.context.getToken(function(token){
			$.ajax({
				url: "/external?download=" + url,
				type: 'GET',
				beforeSend: function (request) {
				    request.setRequestHeader("Authorization", "JWT " + token);
				},
				success: function(response) {
					svgS = response;
					dfd.resolve();
				},
				error: function (request, status, error) {
					dfd.resolve();
				}
			});
		});
	};
	
	
	// find the correct attachment in a set of results
	var findCorrectAttachment = function(results, _page, pageId) {
		console.log("Found %i matching attachments", results.length);
		for(var n = 0; n < results.length; n++) {
			var result = results[n];
			if((_page.length === 0 && result.container.id === pageId) || (_page.length > 0 && result.container.title === _page)) {
				return result._links.download;
			}
		}
		console.log("No matching SVG found");
		return "";
	};
	
	
	// load the settings
	var loadSettings = function(url) {
		var dfd = $.Deferred();
		
		AP.context.getToken(function(token){
			$.ajax({
				url: "/settings",
				type: "GET",
				beforeSend: function (request) {
				    request.setRequestHeader("Authorization", "JWT " + token);
				},
				success: function(response) {
					var responseJSON = JSON.parse(response);
					securityMode = responseJSON.value.securityMode || "internal";					
					whiteList = responseJSON.value.whiteList;
					dfd.resolve();
				},
				error: function () {	
					dfd.resolve();
				}
			});
		});
		
		return dfd.promise();
	};
	
	
	// perform a security check and insert the SVG into the document
	var checkAndInsertSVG = function() {
		
		// init some values
		var allowEmbed = false;
		var svgMD5 = "";
		var svgSHA = "";
		var shaObj = new jsSHA("SHA3-256", "TEXT", { encoding: "UTF8" });
		var allowUnsafe = $(".svg-out:first").data("allow-unsafe") || false;
		
		// perform the selected security check
		if(securityMode === "internal") {
			svgMD5 = md5(svgS);
			shaObj.update(svgS);
			svgSHA = shaObj.getHash("HEX");
			if(allowUnsafe) {
				svgSHA += "-unsafe";
				svgMD5 += "-unsafe";
			}
			if((whiteList.toLowerCase().indexOf(svgMD5) == -1) && (whiteList.toLowerCase().indexOf(svgSHA) == -1)) {
				allowEmbed = performSecurityCheck();
			}
			else {
				allowEmbed = true;	
			}
		}
		else if(securityMode === "none") {
			allowEmbed = true;
		}
		else {
			// we only use the below option if we get a confirmation that it is safe
			//svgS = DOMPurify.sanitize(svgS, {ADD_TAGS: ['foreignObject'], ADD_ATTR: ['pointer-events']});
			svgS = DOMPurify.sanitize(svgS);
			allowEmbed = true;
		}
		
		// negative check
		if(allowEmbed === false) {
			return svgSHA;
		}
		
		// positive check (or not required)
		else {
			addNonces();
			var svg = document.getElementsByClassName("svg-out")[0];
			$(svg).html(svgS);
			sizeCropAndBorder();
			return "ok";
		}
	};
	
	
	// perform an security check on the SVG
	var performSecurityCheck = function() {
		if(!svgBlackCheck()) {
			return false;
		}
		if(!svgWhiteCheck()) {
			return false;
		}
		return true;
	};
	
	
	// first generation checker based on black listing
	var svgBlackCheck = function() {
		// the bad regular expressions we are looking for
		var badRegEx = ["<script", "javascript:", "&#[0-9]+", "&#x[0-9abcdef]+",
		"onfocusin", "onfocusout", "onactivate", "onclick", "onmousedown", "onmouseup", "onmouseover",
		"onmousemove", "onmouseout", "onload", "onunload", "onabort", "onerror", "onresize", "onscroll",
		"onzoom", "onbegin", "onend", "onrepeat", "onkey", "ondblclick", "\\bon[a-z]*="];
	
		// remove all characters we do not want to see as they might falsify the check
		var check = svgS.toLowerCase().replace(/[^ a-z0-9.,;:="'<>+\-\/#\*\(\)&]/g, "");
		
		// loop through all bad regular expressions
		for(var i = 0; i < badRegEx.length; i++) {
			if(check.match(badRegEx[i])) {
				console.log("Negative regex: " + badRegEx[i]);
				return false;
			}
		}
		
		// all fine
		return true;
	};
	
	
	// second generation checker based on white listing
	var svgWhiteCheck = function()
	{
		var svgElements = ["a","animate","animateMotion","animateTransform","circle","clipPath","color-profile","defs","desc","discard","ellipse","feBlend",
			"feColorMatrix","feComponentTransfer","feComposite","feConvolveMatrix","feDiffuseLighting","feDisplacementMap","feDistantLight","feDropShadow",
			"feFlood","feFuncA","feFuncB","feFuncG","feFuncR","feGaussianBlur","feImage","feMerge","feMergeNode","feMorphology","feOffset","fePointLight",
			"feSpecularLighting","feSpotLight","feTile","feTurbulence","filter","foreignObject","g","hatch","hatchpath","image","line","linearGradient",
			"marker","mask","mesh","meshgradient","meshpatch","meshrow","metadata","mpath","path","pattern","polygon","polyline","radialGradient","rect",
			"set","solidcolor","stop","style","svg","switch","symbol","text","textPath","title","tspan","unknown","use","view","div","span"];

		var svgAttributes = ["accent-height","accumulate","additive","alignment-baseline","allowReorder","alphabetic","amplitude","arabic-form","ascent",
			"attributeName","attributeType","autoReverse","azimuth","baseFrequency","baseline-shift","baseProfile","bbox","begin","bias","by","calcMode",
			"cap-height","class","clip","clipPathUnits","clip-path","clip-rule","color","color-interpolation","color-interpolation-filters","color-profile",
			"color-rendering","contentScriptType","contentStyleType","cursor","cx","cy","d","decelerate","descent","diffuseConstant","direction","display",
			"divisor","dominant-baseline","dur","dx","dy","edgeMode","elevation","enable-background","end","exponent","externalResourcesRequired","fill",
			"fill-opacity","fill-rule","filter","filterRes","filterUnits","flood-color","flood-opacity","font-family","font-size","font-size-adjust",
			"font-stretch","font-style","font-variant","font-weight","format","from","fr","fx","fy","g1","g2","glyph-name","glyph-orientation-horizontal",
			"glyph-orientation-vertical","glyphRef","gradientTransform","gradientUnits","hanging","height","href","hreflang","horiz-adv-x","horiz-origin-x",
			"id","ideographic","image-rendering","in","in2","intercept","k","k1","k2","k3","k4","kernelMatrix","kernelUnitLength","kerning","keyPoints",
			"keySplines","keyTimes","lang","lengthAdjust","letter-spacing","lighting-color","limitingConeAngle","local","marker-end","marker-mid",
			"marker-start","markerHeight","markerUnits","markerWidth","mask","maskContentUnits","maskUnits","mathematical","max","media","method","min",
			"mode","name","numOctaves","offset","opacity","operator","order","orient","orientation","origin","overflow","overline-position",
			"overline-thickness","panose-1","paint-order","path","pathLength","patternContentUnits","patternTransform","patternUnits","ping",
			"pointer-events","points","pointsAtX","pointsAtY","pointsAtZ","preserveAlpha","preserveAspectRatio","primitiveUnits","r","radius",
			"referrerPolicy","refX","refY","rel","rendering-intent","repeatCount","repeatDur","requiredExtensions","requiredFeatures","restart",
			"result","rotate","rx","ry","scale","seed","shape-rendering","slope","spacing","specularConstant","specularExponent","speed","spreadMethod",
			"startOffset","stdDeviation","stemh","stemv","stitchTiles","stop-color","stop-opacity","strikethrough-position","strikethrough-thickness",
			"string","stroke","stroke-dasharray","stroke-dashoffset","stroke-linecap","stroke-linejoin","stroke-miterlimit","stroke-opacity","stroke-width",
			"style","surfaceScale","systemLanguage","tabindex","tableValues","target","targetX","targetY","text-anchor","text-decoration","text-rendering",
			"textLength","to","transform","transform-origin","type","u1","u2","underline-position","underline-thickness","unicode","unicode-bidi",
			"unicode-range","units-per-em","v-alphabetic","v-hanging","v-ideographic","v-mathematical","values","vector-effect","version","vert-adv-y",
			"vert-origin-x","vert-origin-y","viewBox","viewTarget","visibility","width","widths","word-spacing","writing-mode","x","x-height","x1","x2",
			"xChannelSelector","y","y1","y2","yChannelSelector","z","zoomAndPan","xmlns"];
		
		var parser = new DOMParser();
		var svgDoc = parser.parseFromString(svgS, "text/xml");
		
		var nodes = svgDoc.getElementsByTagName("*");
		for(var n = 0; n < nodes.length; n++) {
			var node = nodes[n];
			var tagName = node.tagName.toLowerCase();
			if(!/.*[a-z0-9_]:[a-z0-9_].*/.test(tagName)) {
				if(!svgElements.map((e) => {return e.toLowerCase();}).includes(tagName)) {
					console.log("Negative tag name: " + tagName);
					return false;
				}
				var attributes = node.attributes;
				for(var a = 0; a < attributes.length; a++) {
					var attribute = attributes[a];
					var attributeName = attribute.name.toLowerCase();
					if(!/.*[a-z0-9_]:[a-z0-9_].*/.test(attributeName) && !svgAttributes.map((e) => {return e.toLowerCase();}).includes(attributeName)) {
						console.log("Negative attribute name: " + attributeName);
						return false;
					}
				}
			}
		}
		
		return true;
	};
	
	
	var addNonces = function() {
		if(svgS.indexOf("<script") > -1) {
			var noncePool = $("#nonce-pool").data("nonce-pool").trim().split(" ");
			var parser = new DOMParser();
			var svgDoc = parser.parseFromString(svgS, "text/xml");
			var scripts = svgDoc.getElementsByTagName("script");
			for(var n = 0; n < Math.min(scripts.length, 10); n++) {
				var script = scripts[n];
				var nonce = noncePool.pop().replace("'nonce-", "").replace("'", "");
				script.setAttribute("nonce", nonce);
			}
			svgS = svgDoc.documentElement.outerHTML;
		}
	};
	
	
	// apply the desired size to the SVG, crop it if wanted and apply a border
	var sizeCropAndBorder = function() {
	
		var svg = $('.svg-out').find('svg')[0];
		
		// get width and height using CSS (this will always return values)
		var widthInSVG = $(svg).css("width");
		var heightInSVG = $(svg).css("height");
		
		// get the viewBox (if there is one)
		var viewBoxInSVG = svg.getAttribute("viewBox");
		
		// special case: width and height not defined but viewBox
		// while Chrome and Firefox are able to handle this IE is not giving proper results here
		var widthInStyle = false;
		if(svg.hasAttribute("style")) {
			if(svg.getAttribute("style").match(/(^|;) *width:/)) {
				widthInStyle = true;
			}
		}
		if(!widthInStyle && !svg.hasAttribute("width") && svg.hasAttribute("viewBox")) {
			var viewBoxValuesForSize = viewBoxInSVG.split(/,? /, 4);
			widthInSVG = viewBoxValuesForSize[2];
			heightInSVG = viewBoxValuesForSize[3];
		}
		
		// apply the necessary styles
		$(svg).css("overflow", "hidden");
		svg.setAttribute("preserveAspectRatio", "none");
		
		// initial viewBox
		var viewBoxValues = [];
		if(viewBoxInSVG) {
			viewBoxValues = viewBoxInSVG.split(/,? /, 4);
		}
		else {
			viewBoxValues = ["0", "0", widthInSVG.replace("px", ""), heightInSVG.replace("px", "")];
		}
		
		// get crop values
		var leftCrop = parseFloat($('.svg-out').attr("leftCrop"));
		var rightCrop = parseFloat($('.svg-out').attr("rightCrop"));
		var topCrop = parseFloat($('.svg-out').attr("topCrop"));
		var bottomCrop = parseFloat($('.svg-out').attr("bottomCrop"));
		
		// crop the viewBox and set it again
		viewBoxValues[0] = (parseFloat(viewBoxValues[0]) + parseFloat(viewBoxValues[2]) * leftCrop / 100).toString();
		viewBoxValues[1] = (parseFloat(viewBoxValues[1]) + parseFloat(viewBoxValues[3]) * topCrop / 100).toString();
		viewBoxValues[2] = (parseFloat(viewBoxValues[2]) * Math.abs(rightCrop - leftCrop) / 100).toString();
		viewBoxValues[3] = (parseFloat(viewBoxValues[3]) * Math.abs(bottomCrop - topCrop) / 100).toString();
		svg.setAttribute("viewBox", viewBoxValues[0] + " " + viewBoxValues[1] + " " + viewBoxValues[2] + " " + viewBoxValues[3]);
		
		// get requested width and height
		var widthTarget = $('.svg-out').attr("widthTarget");
		var heightTarget = $('.svg-out').attr("heightTarget");
		
		// calculate scale factors for width and height
		// remark: % of available width is handled later
		var scaleX = NaN;
		var scaleY = NaN;
		if((widthTarget.indexOf("px") > -1) || (heightTarget.indexOf("px") > -1)) {
			if(widthTarget.indexOf("px") > -1) {
				scaleX = parseFloat(widthTarget) / parseFloat(widthInSVG) * 100 / Math.abs(rightCrop - leftCrop);
			}
			if(heightTarget.indexOf("px") > -1) {
				scaleY = parseFloat(heightTarget) / parseFloat(heightInSVG) * 100 / Math.abs(bottomCrop - topCrop);
			}
		}
		else if(widthTarget.indexOf("%%") > -1) {
			scaleX = parseFloat("5000px") / parseFloat(widthInSVG) * 100 / Math.abs(rightCrop - leftCrop);
		}
		else {
			scaleX = parseFloat(widthTarget) / 100;
			scaleY = parseFloat(heightTarget) / 100;
		}
		
		// handle missing scale factors
		if(isNaN(scaleX) && isNaN(scaleY)) {
			scaleX = 1;
			scaleY = 1;
		}
		scaleX = isNaN(scaleX) ? scaleY : scaleX;
		scaleY = isNaN(scaleY) ? scaleX : scaleY;
		
		// apply crop values to the scale factors
		scaleX = scaleX * Math.abs(rightCrop - leftCrop) / 100;
		scaleY = scaleY * Math.abs(bottomCrop - topCrop) / 100;
		
		// remove width and height from style
		$(svg).css("width", "");
		$(svg).css("height", "");
		
		// calculate and set new width and height
		var newWidth = (parseFloat(widthInSVG) * scaleX).toString() + "px";
		var newHeight = (parseFloat(heightInSVG) * scaleY).toString() + "px";
		svg.setAttribute("width", newWidth);
		svg.setAttribute("height", newHeight);
		
		// special case % available width scaling
		if(widthTarget.indexOf("%%") > -1) {
			setTimeout(function() {
				svg.setAttribute("width",  widthTarget.replace("%%", "%"));
				svg.removeAttribute("height");
			}, 1000);
		}
		
		// add a border if requested
		if($('.svg-out').attr("border") == "true") {
			$(svg).css("border", "1px solid black");
		}
	};
}