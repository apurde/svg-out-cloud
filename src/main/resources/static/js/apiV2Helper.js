/*jshint esversion: 11 */

function APIV2Helper() {
	
	this.getAttachmentWithContainerAndSpaceFromId = async function(params) {
		try {
			var attachmentRequest = await AP.request("/api/v2/attachments/" + params.attachmentId);
			var attachment = JSON.parse(attachmentRequest.body);
			
			var containerRequest = await AP.request(attachment.pageId ? "/api/v2/pages/" + attachment.pageId : "/api/v2/blogposts/" + attachment.blogPostId);
			var container = JSON.parse(containerRequest.body);
			
			var spaceRequest = await AP.request("/api/v2/spaces/" + container.spaceId);
			var space = JSON.parse(spaceRequest.body);
			
			attachment.container = container;
			attachment.space = space;
			if(params.success) params.success(attachment);
			return attachment;
		}
		catch(error) {
			if(params.error) params.error();
			throw error;
		}
	};
	
	
	this.getContentAndSpaceFromId = async function(params) {
		try {
			var contentType = await getContentTypeFromId(params.contentId);
			var contentAndSpace = contentType === "attachment" ?
				await this.getAttachmentWithContainerAndSpaceFromId({attachmentId : params.contentId}) :
				await getAbstractPageFromId(contentType, params.contentId);
			if(params.success) params.success(contentAndSpace);
			return contentAndSpace;
		}
		catch(error) {
			if(params.error) params.error();
			throw error;
		}
	};
	
	
	this.getAttachmentsForContentId = async function(params) {
		try {
			var contentType = await getContentTypeFromId(params.contentId);
			var attachmentsRequest = await AP.request((contentType === "page" ? "/api/v2/pages/" :  "/api/v2/blogposts/") + params.contentId + "/attachments");
			var attachments = JSON.parse(attachmentsRequest.body);
			if(params.success) params.success(attachments);
			return attachments;
		}
		catch(error) {
			if(params.error) params.error();
			throw error;
		}
	};
	
	
	var getContentTypeFromId = async function(contentId) {
		try {
			var typeRequest = await AP.request({
				"url": "/api/v2/content/convert-ids-to-types",
				"type": "POST",
				"contentType": "application/json; charset=utf-8",
				"dataType" : "json",
				"data" : JSON.stringify({
					"contentIds" : [contentId]
				})
			});
			return JSON.parse(typeRequest.body).results[contentId];
		}
		catch(error) {
    		throw error;
		}
	};
	
	
	var getAbstractPageFromId = async function(contentType, contentId) {
		try {
			var abstractPageRequest = await AP.request(contentType === "page" ? "/api/v2/pages/" + contentId : "/api/v2/blogposts/" + contentId);
			var abstractPage = JSON.parse(abstractPageRequest.body);
			
			var spaceRequest = await AP.request("/api/v2/spaces/" + abstractPage.spaceId);
			var space = JSON.parse(spaceRequest.body);
			
			abstractPage.space = space;
			return abstractPage;
		}
		catch(error) {
    		throw error;
		}
	};
}


var apiV2Helper = new APIV2Helper();