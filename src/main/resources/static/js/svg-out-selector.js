function svgSelector() {
	
	var lastClickX = 0;
	var lastClickY = 0;
	var lastElement = 0;
	var listOfElements = [];
	var svg = null;
	var callback = null;
	
	
	// start the element selection mode
	this.start = function(callbackP) {
		
		// our SVG
		svg = $("#preview").find("svg")[0];
		
		// save the callback
		callback = callbackP;
		
		// remove pointer-events if present
		$(svg).children().removeAttr("pointer-events");
		
		// register the event listener
		svg.addEventListener('click', clickInSVG);
	};
	
	
	// stop the element selection mode
	this.stop = function() {
		
		removeSelectionRect();
		svg.removeEventListener('click', clickInSVG);
	};
	
	
	// select element with Id or remove rect if Id is empty
	this.showElementWithId = function(id) {
		
		if(!svg) { return; }
		
		if(id !== "") {
			var e = document.getElementById(id);
			if(e) {
				markElement(e);
				return;
			}
		}
		removeSelectionRect();
	};
	
	
	// show first element which matches a link
	this.showElementWithLink = function(linkText) {
		
		if(!svg) { return; }
		
		// the xlink name space; we need this for appending the node
		var  xlinkns = "http://www.w3.org/1999/xlink";
		
		// get all links in the SVG
		var linksInSVG = svg.getElementsByTagName("a");
		
		// loop through then
		for(var n = 0; n < linksInSVG.length; n++) {
			
			// is it a link we are searching for
			if(linksInSVG[n].hasAttributeNS(xlinkns, "href")) {
				if(linksInSVG[n].getAttributeNS(xlinkns, "href") == linkText) {
					
					// mark it
					markElement(linksInSVG[n]);
					return;
				}
			}
		}
		
		// nothing found => remove a potential selection rect
		removeSelectionRect();
	};
	
	
	// add an element into the SVG
	var addRect = function(x, y, w, h) {
		
		// prepare the style
		var style = 'fill:MediumBlue;stroke:MediumBlue;stroke-width:$1;fill-opacity:0.1;stroke-opacity:0.9;stroke-dasharray:$2 $3';
		var widthOfSVG = parseFloat($(svg).css("width"));
		var viewBoxValues = svg.getAttribute("viewBox").split(' ', 4);
		var strokeWidth = Math.round(5 * viewBoxValues[2] / widthOfSVG);
		style = style.replace("$1", strokeWidth).replace("$2", strokeWidth * 2).replace("$3", strokeWidth);
		
		// define a rect
		var rect = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
		rect.setAttribute('x', x);
		rect.setAttribute('y', y);
		rect.setAttribute('width', w);
		rect.setAttribute('height', h);
		rect.setAttribute('id', 'svgOutSelector');
		rect.setAttribute('style', style);

		// remove old rect
		removeSelectionRect();

		// add the rect to the SVG
		svg.appendChild(rect);
	};
	
	
	// remove the (old) selection rect
	var removeSelectionRect = function() {
		var selRect = svg.getElementById('svgOutSelector');
		if(selRect) {
			selRect.parentNode.removeChild(selRect);
		}
	};
	
	
	// react on a click in the SVG
	var clickInSVG = function(evt) {
		
		evt.preventDefault();
		
		// click at new position
		if(Math.abs(lastClickX - evt.clientX) > 5 || Math.abs(lastClickY - evt.clientY) > 5) {
			listOfElements = allElementsFromPoint(evt.clientX, evt.clientY);
			lastElement = 0;
			lastClickX = evt.clientX;
			lastClickY = evt.clientY;
		}
		
		// click at same position
		else {
			lastElement = lastElement + 1;
			if(lastElement >= listOfElements.length) {
				lastElement = 0;
			}
		}
		
		if(listOfElements.length > 0) {
			
			// add rect around the selected SVG element
			markElement(listOfElements[lastElement]);
			
			// run the callback
			callback(listOfElements[lastElement]);
		}
	};
	
	
	// mark an element
	var markElement = function(e) {
		var svgRect = svg.getBoundingClientRect();
		var rect = e.getBoundingClientRect();
		var viewBoxValues = svg.getAttribute("viewBox").split(' ', 4);
		var sx = (rect.left - svgRect.left) * parseFloat(viewBoxValues[2]) / svgRect.width + parseFloat(viewBoxValues[0]);
		var sy = (rect.top - svgRect.top) * parseFloat(viewBoxValues[3]) / svgRect.height + parseFloat(viewBoxValues[1]);
		var sw = rect.width * parseFloat(viewBoxValues[2]) / svgRect.width;
		var sh = rect.height * parseFloat(viewBoxValues[3]) / svgRect.height;
		addRect(sx, sy, sw, sh);
	};
	
	
	// get all elements at the given point (top-most is first)
	var allElementsFromPoint = function(x, y) {
		
	    var rawElements = $(svg).find("*");
		var elements = [];
		
		for(var e = 0; e < rawElements.length; e++) {
			var element = rawElements[e];
        	if((element.id != "svgOutSelector") && (element.parentNode.tagName.toLowerCase() != "a")) {
        		var r = element.getBoundingClientRect();
        		if(r.left <= x && r.left + r.width >= x && r.top <= y && r.top + r.height >= y) {
        			elements.push(element);
        		}
        	}
		}
	
    	// return the elements we found
	    return elements.reverse();
	};
}