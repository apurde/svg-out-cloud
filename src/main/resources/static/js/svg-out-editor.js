function svgEditor() {
	
  	var linkList = [];
	var lastSelectedLink = -1;
	var currentPageId = 0;
	var currentPageResult = [];
	var currentSearchTerm = "";
	var baseUrl = "";
	var messageFindAttachment = "";
	var messageSecurityCheck = "";
	var previewDeferred = null;
	var exportDeferred = null;
	var md5ShortOld = "";
	var linkSelectionMode = false;
	var selector = new svgSelector();
	
	
	// ***** public functions *****

	this.initGui = function(macroParams, attPlaceholder, pbaseUrl) {
		initGuiI(macroParams, attPlaceholder, pbaseUrl);
	};
	
	this.guiToMacroParams = function() {
		return guiToMacroParamsI();
	};
	
	this.addLink = function(defaultText) {
		addLinkI(defaultText);
	};
	
	this.removeLink = function() {
		removeLinkI();
	};
	
	this.changeLink = function() {
		changeLinkI();
	};
	
	this.updateListEntry = function() {
		updateListEntryI();
	};
	
	this.linkTypeChange = function() {
		linkTypeChangeI();
	};
				
	this.runPreview = function(loader, pMessageFindAttachment, pMessageSecurityCheck) {
		return runPreviewI(loader, pMessageFindAttachment, pMessageSecurityCheck);
	};
	
	this.generatePNG = function(loader, pMessageFindAttachment, pMessageSecurityCheck, messageExportTitle, messageExport) {
		return generatePNGI(loader, pMessageFindAttachment, pMessageSecurityCheck, messageExportTitle, messageExport);
	};
	
	this.linkSelectorClick = function(loader, messageFindAttachment, messageSecurityCheck) {
		linkSelectorClickI(loader, messageFindAttachment, messageSecurityCheck);
	};
			
	
	// ***** internal functions  *****

	
	// Confluence attachment load local page
	var confluenceAttachmentLoadLocal = function() {
		AP.navigator.getLocation(function (location) {
			currentPageId = location.context.contentId;
			apiV2Helper.getAttachmentsForContentId({
				contentId: location.context.contentId,
				success: function(response){
					for(var n = 0; n < response.results.length; n++) {
						if(response.results[n].title.toLowerCase().indexOf('.svg') > -1) {
							var element = {
								id: response.results[n].id,
								text: response.results[n].title,
								download: response.results[n]._links.download
								};
							currentPageResult.push(element);
						}
					}
				},
				error: function() {
					console.log(arguments);
				}
			});
		});
	};
	
	
	// Confluence attachment load
	var confluenceAttachmentLoad = function(attachmentID, attPlaceholder) {
	 		
		$("#attachment").auiSelect2({
		
			// let the user enter three letters before we search
		    minimumInputLength: 3,
		    
		    // set the placeholder
		    placeholder: attPlaceholder,

			containerCssClass: "wrap",
		    
		    // let select2 load matching titles using CQL in a rest call
		    ajax: {
		        url: '/rest/api/content/search',
		        dataType: 'json',
		        type: "GET",
		        data: function (term) {
		        	currentSearchTerm = term;
		            return {
		                cql: "(title~'"+ term + "*') AND (type=attachment)",
		                expand: "container",
		            };
		        },
		        // map the results to a select2 format
		        results: function (responseJSON) {
		        	var response = JSON.parse(responseJSON);
		        	var filteredResult = [];
					for(var n = 0; n < response.results.length; n++) {
						if(response.results[n].title.toLowerCase().indexOf('.svg') > -1) {
							var element = {
								id: response.results[n].id,
								text: response.results[n].title + " (" + response.results[n].container.title + ")",
								download: response.results[n]._links.download
								};
							filteredResult.push(element);
						}
					}
					var filteredResultJSON = JSON.stringify(filteredResult);
					for(n = 0; n < currentPageResult.length; n++) {
						if((filteredResultJSON.indexOf(currentPageResult[n].id) == -1) &&
							(currentPageResult[n].text.toLowerCase().indexOf(currentSearchTerm.toLowerCase()) > -1))  {
							filteredResult.push(currentPageResult[n]);
						}
					}
					return {results : filteredResult };
		        },
		        // we need a modified transport layer to handle proper signing of the request
		        transport: function transport(params) {
	                AP.request({
	                    url: params.url,
	                    headers: {
	                        "Accept": "application/json"
	                    },
	                    data: params.data,
	                    success: params.success,
	                    error: params.error
	                });
	    		}	
		    },
		    
		    // the initial value in the box
		    initSelection: function (element, callback) {
	    		if(attachmentID.length > 0) {
					apiV2Helper.getAttachmentWithContainerAndSpaceFromId({
						attachmentId : attachmentID,
						success: function(response){
							var confContent = {"id" : response.id, "text" : response.title + " (" + response.container.title + ")", "download" : response._links.download};
							callback(confContent);
						},
						error: function() {
							console.log(arguments);
						}
					});
				}	
		    }
		}).auiSelect2('val', []);
	};
	
		
	// populate link list
	var populateLinkList = function() {
		for(var n = 0; n < linkList.length; n++) {
			var option = document.createElement("option");
			option.text = linkList[n].linkToReplace;
			document.getElementById("linkselect").add(option);
		}
		if(linkList.length > 0) {
			document.getElementById("linkselect").selectedIndex = 0;
		}
		changeLinkI();
	};
	
	
	// add link
	var addLinkI = function(defaultText) {
		
		// add the the link to the select element in HTML
		var option = document.createElement("option");
		option.text = defaultText;
		document.getElementById("linkselect").add(option);
		
		// create a new link in our internal list
		var link = {
			linkToReplace : defaultText,
			type : 0,
			linkConf : "",
			linkExt : "",
			anchor : "",
			tooltip : ""
			};
		linkList.push(link);
		
		document.getElementById("linkselect").selectedIndex = document.getElementById("linkselect").options.length - 1;
		
		// trigger the change-link-function to update all of our fields
		changeLinkI();
	};
	
	
	// remove link
	var removeLinkI = function() {
	
		// get the index to remove
		var selectedIndex = document.getElementById("linkselect").selectedIndex;
		if(selectedIndex > -1) {
		
			// remove it visually from the select HTML element
			document.getElementById("linkselect").remove(selectedIndex);
			
			// remove the link from our internal list
			linkList.splice(selectedIndex, 1);
			
			// the last selected was removed => -1
			lastSelectedLink = -1;
			
			// move the selected index to an entry which exists
			if(selectedIndex > document.getElementById("linkselect").options.length - 1) {
				selectedIndex = document.getElementById("linkselect").options.length - 1;
			}
			document.getElementById("linkselect").selectedIndex = selectedIndex;
			
			// trigger the change-link-function to update all of our fields
			changeLinkI();
		}
	};
	
	
	// handle the change of the selected link
	var changeLinkI = function() {
		
		var link = null;
		
		// save the data from the previously selected link
		if(lastSelectedLink > -1) {
			link = {
				linkToReplace : $('#linkToReplace').val(),
				type : $('#type').prop('selectedIndex'),
				linkConf : $('#linkConf').val(),
				linkExt : $('#linkExt').val(),
				anchor : $('#anchor').val(),
				tooltip : $('#tooltip').val()
			};
			linkList[lastSelectedLink] = link;
		}
		
		// show the data from the newly selected link (might be real one or -1)
		var selectedIndex = document.getElementById("linkselect").selectedIndex;
		if(selectedIndex > -1) {
			link = linkList[selectedIndex];
			$('#linkToReplace').val(link.linkToReplace);
			$('#type').prop('selectedIndex', link.type);
			$('#linkConf').val(link.linkConf);
			$('#linkExt').val(link.linkExt);
			$('#anchor').val(link.anchor);
			$('#tooltip').val(link.tooltip);
			lastSelectedLink = selectedIndex;
			$('.linkNeeded').prop("disabled", false);
			$('#linkPickerLink').css("pointer-events", "");
		}
		else {
			$('#linkToReplace').val("");
			$('#type').prop('selectedIndex', 0);
			$('#linkConf').val("");
			$('#linkExt').val("");
			$('#anchor').val("");
			$('#tooltip').val("");
			$('.linkNeeded').prop("disabled", true);
			$('#linkPickerLink').css("pointer-events", "none");
		}
		
		// ensure that the right fields for the link type are shown
		linkTypeChangeI();
		
		// init our select2 field which dynamically loads Confluence content titles
		confluenceContentLoad();
		
		// show the added link in the preview (in case we are in right mode)
		if(linkSelectionMode) {
			showSelectedElement();
		}
	};
	
	
	// update text in list when user changes the input field
	var updateListEntryI = function() {
		var selectedIndex = document.getElementById("linkselect").selectedIndex;
		if(selectedIndex > -1) {
			document.getElementById("linkselect").options[selectedIndex].text = $('#linkToReplace').val();
		}
	};
	
	
	// react on a link type change (external to Confluence and vice versa)
	var linkTypeChangeI = function() {
	
		// external link
		if(document.getElementById("type").selectedIndex > 1) {
			$('#linkConfluence').hide();
			$('#linkExternal').show();
		}
		
		// Confluence link
		else {
			$('#linkConfluence').show();
			$('#linkExternal').hide();
		}
	};
	
	
	// Confluence content load
	var confluenceContentLoad = function() {
	 		
		$("#linkConf").auiSelect2({
		
			// let the user enter three letters before we search
		    minimumInputLength: 3,
		    
		    // let select2 load matching titles using CQL in a rest call
		    ajax: {
		        url: '/rest/api/content/search',
		        dataType: 'json',
		        type: "GET",
		        data: function (term) {
		            return {
		                cql: "title~'"+ term + "*'",
		                expand: "space"
		            };
		        },
		        // map the results to a select2 format
		        results: function (responseJSON) {
		        	var response = JSON.parse(responseJSON);
		            return {
		                results: $.map(response.results, function (item) {
		                    return {
		                        text: item.title + " (" + item.space.key + ")",
		                        id: item.id
		                    };
		                })
		            };
		        },
		        // we need a modified transport layer to handle proper signing of the request
		        transport: function transport(params) {
	                AP.request({
	                    url: params.url,
	                    headers: {
	                        "Accept": "application/json"
	                    },
	                    data: params.data,
	                    success: params.success,
	                    error: params.error
	                });
	    		}	
		    },
		    
		    // the initial value in the box
		    initSelection: function (element, callback) {
		    	var selectedIndex = document.getElementById("linkselect").selectedIndex;
		    	if(selectedIndex > -1) {
		    		var contentID = linkList[selectedIndex].linkConf;
		    		if(contentID.length > 0) {
						apiV2Helper.getContentAndSpaceFromId({
							contentId : contentID,
							success: function(response){
								var confContent = {"id" : response.id, "text" : response.title + " (" + response.space.key + ")"};
								callback(confContent);
							},
							error: function() {
								console.log(arguments);
							}
						});
					}
		 		}	
		    }
		});
	};
	
	
	// initialize GUI
	var initGuiI = function(macroParams, attPlaceholder, pbaseUrl) {
		
		$("#previewLoad").hide();
				
		// store values for later user
		baseUrl = pbaseUrl;
		
		// attachment
		var attachmentID = macroParams && macroParams.attachmentID || "";
		var _page = macroParams && macroParams.page || "";
		var _name = macroParams && macroParams.name || "";
		if(_name.length > 0) {
			_page = _page.replace(/.*title=/, "").replace(/,.*/, "");
			_name = _name.replace(/.*filename=/, "").replace(/,.*/, "");
			findAttachmentIdOfMigratedSVG(_name, _page, function(foundAttachmentID) {
				attachmentID = foundAttachmentID;
				console.log(attachmentID);
				confluenceAttachmentLoad(attachmentID, attPlaceholder);
			});
		}
		else {
			confluenceAttachmentLoad(attachmentID, attPlaceholder);
		}
		
		// Server and Data Center migration
		if(macroParams) {
			if(macroParams.align === "left") {
				macroParams.align = 0;
			}
			if(macroParams.align === "center") {
				macroParams.align = 1;
			}
			if(macroParams.align === "right") {
				macroParams.align = 2;
			}
			if(macroParams.panZoom === undefined && macroParams.panAndZoom === true) {
				macroParams.panZoom = true;
			}
			if(macroParams.width === undefined && macroParams.scaleX) {
				macroParams.width = macroParams.scaleX;
			}
			if(macroParams.height === undefined && macroParams.scaleY) {
				macroParams.height = macroParams.scaleY;
			}
		}
		
		// the simple stuff
		$('#width').val(macroParams && macroParams.width || "");
		$('#height').val(macroParams && macroParams.height || "");
		$('#align').prop('selectedIndex', macroParams && macroParams.align || 0);
		$('#top').val(macroParams && macroParams.top || "0%");
		$('#bottom').val(macroParams && macroParams.bottom || "100%");
		$('#left').val(macroParams && macroParams.left || "0%");
		$('#right').val(macroParams && macroParams.right || "100%");
		$('#panZoom').prop("checked", (( macroParams && macroParams.panZoom || "false") == "true"));
		$('#startActive').prop("checked", (( macroParams && macroParams.startActive || "true") == "true"));
		$('#useWheel').prop("checked", (( macroParams && macroParams.useWheel || "true") == "true"));
		$('#buttons').prop("checked", (( macroParams && macroParams.zoomButtons || "false") == "true"));
		$('#minZoom').val(macroParams && macroParams.minZoom || "0.1");
		$('#maxZoom').val(macroParams && macroParams.maxZoom || "10");
		$('#border').prop("checked", (( macroParams && macroParams.border || "false") == "true"));
		$('#allowUnsafe').prop("checked", (( macroParams && macroParams.allowUnsafe || "false") == "true"));
		$('#exportPNG').prop("checked", (( macroParams && macroParams.exportPNG || "false") == "true"));
		$("#svgUrl").val(macroParams && macroParams.svgUrl || "");
		$('#linkHighlight').prop("checked", (( macroParams && macroParams.linkHighlight || "false") == "true"));
		$('#linkHighlightHover').prop("checked", (( macroParams && macroParams.linkHighlightHover || "true") == "true"));
		$("#rasterResolution").val(macroParams && macroParams.rasterResolution || "1.0");
			
		// the link list
		var linksJSON = macroParams && macroParams.linkList || "[]";
		linkList = JSON.parse(linksJSON);
		populateLinkList();
		
		// was exportPNG set?
		if(!macroParams || !macroParams.exportPNG) {
			initPNGExport();
		}
		
		// the old MD5
		md5ShortOld = macroParams && macroParams.md5Short || "noOldMD5Short";
		
		// get the attachments of the current page
		confluenceAttachmentLoadLocal();
		
		$("#attachment,#svgUrl").change(function() {
			$("#preview").empty();
			turnOffLinkSelectionMode();
		});
		
		$("#resetAttachment").click(function() {
			$('#attachment').val(null).trigger('change');
		});
	};
	
	
	// find the attachment ID of a migrated SVG
	var findAttachmentIdOfMigratedSVG = function(_name, _page, callback) {
		AP.navigator.getLocation(function(location) {
			var pageId = location.context.contentId.toString();
			console.log("Looking for %s and page %s starting from current page %s", _name, _page, pageId);
			AP.request({
				url: '/rest/api/content/search?expand=container&cql=title="' + _name + '" and type=attachment',
				success: function(resultText) {
					var results = JSON.parse(resultText).results;
					console.log("Found %i attachments with matching name", results.length);
					for(var n = 0; n < results.length; n++) {
						var result = results[n];
						if((_page.length === 0 && result.container.id === pageId) || (_page.length > 0 && result.container.title === _page)) {
							callback(result.id);
							return;
						}
					}
					console.log("No matching attachment found");
					callback("");
				},
				error: function() {
					console.log("Error searching for matching attachments");
					callback("");
				}
			});
		});
	};
	
	
	// store the values from the dialog GUI into an object
	var guiToMacroParamsI = function() {
		
		// ensure that we get the latest changes on the link tab
		changeLinkI();
	
		var linksJSON = JSON.stringify(linkList);
		
		var macroParams = {
			attachmentID: $('#attachment').val(),
			width: $('#width').val(),
			height: $('#height').val(),
			align: $('#align').prop('selectedIndex'),
			top: $('#top').val(),
			bottom: $('#bottom').val(),
			left: $('#left').val(),
			right: $('#right').val(),
			panZoom: $('#panZoom').prop("checked") ? "true" : "false",
			startActive: $('#startActive').prop("checked") ? "true" : "false",
			useWheel: $('#useWheel').prop("checked") ? "true" : "false",
			zoomButtons: $('#buttons').prop("checked") ? "true" : "false",
			minZoom: $('#minZoom').val(),
			maxZoom: $('#maxZoom').val(),
			border: $('#border').prop("checked") ? "true" : "false",
			allowUnsafe: $('#allowUnsafe').prop("checked") ? "true" : "false",
			exportPNG: $('#exportPNG').prop("checked") ? "true" : "false",
			linkList: linksJSON,
			svgUrl: $('#svgUrl').val(),
			linkHighlight: $('#linkHighlight').prop("checked") ? "true" : "false",
			linkHighlightHover: $('#linkHighlightHover').prop("checked") ? "true" : "false",
			rasterResolution: document.getElementById('rasterResolution').value					
			};
		
		var source = macroParams.attachmentID.length > 0 ? macroParams.attachmentID : macroParams.svgUrl;
		var rr = macroParams.rasterResolution !== "1.0" ? macroParams.rasterResolution : "";
		macroParams.md5Short = md5(source + rr + macroParams.width + macroParams.height +
			macroParams.left + macroParams.right + macroParams.top + macroParams.bottom).substring(0, 5);
					
		return macroParams;
	};	
	

	// show a preview
	var runPreviewI = function(loader, pMessageFindAttachment, pMessageSecurityCheck) {
		
		// save some parameters for later when the callback is called
		messageFindAttachment = pMessageFindAttachment;
		messageSecurityCheck = pMessageSecurityCheck;
		
		// start spinner
		$('#previewLoad').show();
					
		// get the current params
		var macroParams = guiToMacroParamsI();
		
		// prepare the div section holding the SVG later on
		var alignStyle = "";
		switch(macroParams.align) {
			case 0: alignStyle = "style='text-align:left'"; break;
			case 1: alignStyle = "style='text-align:center'"; break;
			case 2: alignStyle = "style='text-align:right'"; break;
		}
		var section = "<div class='svg-out' widthTarget='" + sanitize(macroParams.width) + "' heightTarget='" +  sanitize(macroParams.height) +
			"' leftCrop='" +  sanitize(macroParams.left) + "' rightCrop='" +  sanitize(macroParams.right) + " 'topCrop='" +  sanitize(macroParams.top) + "' bottomCrop='" +  sanitize(macroParams.bottom) +
			"' border='" +  macroParams.border +"'  panAndZoom='" + macroParams.panZoom +
			"' useWheel='" +  macroParams.useWheel + "' startActive='" +  macroParams.startActive + "' zoomButtons='" +  macroParams.zoomButtons +
			"' minZoom='" +  sanitize(macroParams.minZoom) + "' maxZoom='" +  sanitize(macroParams.maxZoom) + "' " + alignStyle +
			"' attachmentID='" +  macroParams.attachmentID + "' linkList='" +  macroParams.linkList + "' md5Short='" +  macroParams.md5Short +
			"' linkHighlight='" +  macroParams.linkHighlight + "' linkHighlightHover='" +  macroParams.linkHighlightHover +
			"' rasterResolution='" +  macroParams.rasterResolution + "' svgUrl='" +  macroParams.svgUrl + "'></div>";
		
		// set this div as the innererHTML of our preview div
		document.getElementById("preview").innerHTML=section;
		
		// load and insert the SVG
		loader.loadAndInsertSVG(previewFinished);
		
		previewDeferred = $.Deferred();
		return previewDeferred.promise();
	};
	
	
	// callback when preview is finished
	var previewFinished = function(result) {
	
		// success
		if(result == "ok") {
			panZoom();
			if(linkSelectionMode) {
				replaceLinks(baseUrl, false);
				enableLinks();
				selector.start(takeLinkOrIdFromSelection);
				showSelectedElement();
			}
			else {
				replaceLinks(baseUrl, true);
				disableLinks();
			}
		}
		
		// could not load attachment
		else if(result == "could_not_load") {
			document.getElementsByClassName("svg-out")[0].innerHTML = "<p>" + messageFindAttachment + "</p>";
		}
		
		// negative security check
		else {
			document.getElementsByClassName("svg-out")[0].innerHTML = messageSecurityCheck.replace(/<a.*?>/, "").replace("</a>", "") + " " + result;
		}
		
		// stop spinner
		$('#previewLoad').hide();
		
		return previewDeferred.resolve();
	};
	
	
	// disable all links in the SVG
	var disableLinks = function() {
		var svg = $(".svg-out").find("svg")[0];
		$(svg).find("a").bind("click", false);
	};
	
	
	// enable all links in the SVG
	var enableLinks = function() {
		var svg = $(".svg-out").find("svg")[0];
		$(svg).find("a").unbind("click", false);
	};
	
	
	// generate a PNG for export
	var generatePNGI = function(loader, pMessageFindAttachment, pMessageSecurityCheck, messageExportTitle, messageExport) {
		
		exportDeferred = $.Deferred();
		
		// get the latest parameters
		var macroParams = guiToMacroParamsI();
		
		// turn off link selection mode
		turnOffLinkSelectionMode();
		
		// check if conversion is needed
		var converter = new svg2PNGConverter();
		converter.checkIfConversionNeeded(macroParams.exportPNG, macroParams.attachmentID, macroParams.md5Short, md5ShortOld, currentPageId, function(isNeeded, filename, url) {
			
			// conversion needed
			if(isNeeded) {
				
				// show a working flag
				var workingFlag = AP.flag.create({
				    type: 'info',
				    title: messageExportTitle,
				    body: messageExport
				});
				
				// run a preview first
				runPreviewI(loader, pMessageFindAttachment, pMessageSecurityCheck).then(function() {
					
					// then convert
					converter.convert(loader.getSVG(), baseUrl, filename, url).then(function(){
						
						// when conversion is finished
						workingFlag.close();
						return exportDeferred.resolve();
					});
					
					return exportDeferred.promise();
				});
			}
			
			// not needed
			else {
				return exportDeferred.resolve();
			}
		});
		
		return exportDeferred.promise();
	};
	
	
	// init the exportPNG checkbox based on the settings
	var initPNGExport = function() {
		AP.context.getToken(function(token){
			$.ajax({
				url: "/settings",
				type: "GET",
				beforeSend: function (request) {
				    request.setRequestHeader("Authorization", "JWT " + token);
				},
				success: function(response) {
					var responseJSON = JSON.parse(response);
					if(responseJSON.value.exportPNGDefault === "true") {
						$('#exportPNG').prop("checked", true);
					}
				}
			});
		});
	};
	
	
	// take the selected SVG element into the input field
	var takeLinkOrIdFromSelection = function(element) {
		
		// the xlink name space; we need this for appending the node
		var  xlinkns = "http://www.w3.org/1999/xlink";
				
		// is it a link?
		if(element.tagName == 'a') {
			
			// take the link (for legacy reasons)
			if(element.hasAttributeNS(xlinkns, "href")) {
				$('#linkToReplace').val(element.getAttributeNS(xlinkns, "href"));
			}
			else {
				$('#linkToReplace').val('undefined');
			}
		}
		
		// not a link
		else {
			// take the id
			$('#linkToReplace').val("id:" + element.id);
		}
		
		updateListEntryI();
	};
	
	
	// link selection button clicked
	var linkSelectorClickI = function(loader, messageFindAttachment, messageSecurityCheck) {
		
		// in case it was turned off
		if(!linkSelectionMode) {
			
			// enable the mode
			linkSelectionMode = true;
			
			// set the background color of the button
			$('#linkSelector').css('background-color', 'LightGreen');
			
			// run a preview
			// remark: the actual activation (selector.start) will be done in previewFinished
			runPreviewI(loader, messageFindAttachment, messageSecurityCheck);
		}
		
		// in case it was turned on => turn it off
		else {
			selector.stop();
			turnOffLinkSelectionMode();
			runPreviewI(loader, messageFindAttachment, messageSecurityCheck);
		}
	};
	
	
	// turn off link selection mode
	var turnOffLinkSelectionMode = function() {
		linkSelectionMode = false;
		$('#linkSelector').css('background-color', '');
		disableLinks();
	};
	
	
	// show selected element
	var showSelectedElement = function() {
		if($('#linkToReplace').val().indexOf("id:") == 0) {
			selector.showElementWithId($('#linkToReplace').val().replace("id:", ""));
		}
		else {
			selector.showElementWithLink($('#linkToReplace').val());
		}
	};
	
	
	// sanitize values we embed in HTML
	var sanitize = function(value) {
		return value.replace(/[<>'"&#]/g, "");
	};
}


var editor = new svgEditor();
var loader = new svgLoader();
var messageFindAttachment = $("#errorFind").text();
var messageSecurityCheck = $("#securityCheck").text();


// bind actions formly done inline
$("#run-preview").click(function() {
	runPreview();
});

$("#add-link").click(function() {
	editor.addLink($("#defaultLinkName").text());
});

$("#remove-link").click(function() {
	editor.removeLink();
});

$("#linkSelector").click(function() {
	linkSelectorClick();
});

$("#linkToReplace").focusout(function() {
	editor.updateListEntry();
});

$("#type").change(function() {
	editor.linkTypeChange();
});

$("#linkselect").change(function() {
	editor.changeLink();
});

    	
// when user presses the preview link
function runPreview() {
	editor.runPreview(loader, messageFindAttachment + " / " + $("#compressedDraft").text(), messageSecurityCheck);
}


// when the user presses the link selection key
function linkSelectorClick() {
	editor.linkSelectorClick(loader, messageFindAttachment, messageSecurityCheck);	
}


function finishAndLeave() {

	// get the current parameters
	var macroParams = editor.guiToMacroParams();
		
	// do we have a valid attachmentID?
	if((macroParams.attachmentID && macroParams.attachmentID.length > 0) || (macroParams.svgUrl && macroParams.svgUrl.length > 0)) {
	
		// disable all elements
		$('#paramDivID *').attr('disabled', true);
		AP.dialog.getButton("submit").disable();
		
		// save macro parameters
		AP.confluence.saveMacro(macroParams);
	
		// the things we need to do before we close
		if(macroParams.attachmentID && macroParams.attachmentID.length > 0) {
			$.when(
				editor.generatePNG(loader, messageFindAttachment, messageSecurityCheck,  $("#genPNGTitle").text(), $("#genPNG").text())
				
			// and then close	
			).done(function() {
				// good bye
				AP.confluence.closeMacroEditor();
				return true;
			});
		}
		else {
			AP.confluence.closeMacroEditor();
			return true;
		}
	}
	return false;
}

	 	
// interface to the dialog

// init the dialog
AP.confluence.getMacroData(function(macroParams) {
	var attPlaceholder = $("#attachmentPlaceholder").text();
	editor.initGui(macroParams, attPlaceholder, $("body").data("base-url"));
});	

// store macro parameters onSubmit
function onSubmit() {
	finishAndLeave();
}

AP.dialog.disableCloseOnSubmit();
AP.events.on('dialog.button.click', function(data){
	if(data.button.name === 'submit') {
		onSubmit();
	}
});