function svg2PNGConverter() {
	
	var dfd = null;
	
	
	// convert the SVG to a PNG
	this.convert = function(svgS, cCLink, filename, url) {
		
		dfd = $.Deferred();
		return convert2PNGBase64(svgS, cCLink, filename, url);
	};
	
	
	// check if a conversion is necessary
	this.checkIfConversionNeeded = function(exportPNG, attachmentID, md5Short, md5ShortOld, currentPageId, callback) {
		
		// no conversion in case the flag is false
		if(exportPNG == "false") {
			callback(false, "", "");
			return;
		}
		
		AP.request({
			url: '/api/v2/attachments/' + attachmentID,
			success: function(resultText) {
				var result = JSON.parse(resultText);
				
				var download = result._links.download;
				var version = result.version.number;
				
				// determine modified SVG filename
				var svgFileName = matcher(download, /(?:attachments\/.*?\/)(.*?)(?:\.svgz?(\?|$))/i);
				svgFileName = decodeURIComponent(svgFileName);
				svgFileName = svgFileName.replace(/[^a-zA-Z0-9]/g,"");
				
				// generate URL
				var url = "/rest/api/content/" + currentPageId + "/child/attachment";
				var urlV2 = "/api/v2/pages/" + currentPageId + "/attachments";
				
				// generate filename
				var filename = md5Short + "_" + version + "_" + svgFileName + ".png";
				
				// get all attachments for the pageID
				AP.request({
		            url: urlV2,
		            type: "GET",
		             success: function (responseText) {
		            	 // already there?
		            	 if(responseText.indexOf(filename) > -1) {
		            		 callback(false, filename, url);
		            	 }
		            	 // not there?
		            	 else {
		            		 checkForOldPNG(responseText, filename, urlV2, md5Short, md5ShortOld);
		            		 callback(true, filename, url);
		            	 }
		            },
		            error: function (request, status, error) {
		            	callback(false, filename, url);
						console.log("Could not get attachments!");
					}
		        });
			}
		});
	};
	
	
	// take the data from the DOM do check whether a conversion is needed
	this.checkIfConversionNeededFromDOM = function(currentPageId, callback) {
		
		// get the relevant data from the svg-out container
		var mySVGC = $('.svg-out')[0];
		var attachmentID = mySVGC.getAttribute("attachmentID");
		var md5Short = mySVGC.getAttribute("md5Short");
		var exportPNG = mySVGC.getAttribute("exportPNG");
		
		this.checkIfConversionNeeded(exportPNG, attachmentID, md5Short, "noOldMD5Short", currentPageId, callback);
	};
	
	
	// check for an old version of the related PNG and delete it
	var checkForOldPNG = function(responseText, filename, url, md5Short, md5ShortOld) {
		
		// responseText to a JSON array
		var response = JSON.parse(responseText);
				
		// through all attachments
		for(var n = 0; n <  response.results.length; n++) {
			
			// our MD5 but not our file?
			if((response.results[n].title.indexOf(md5Short + "_") == 0) &&
				(response.results[n].title.indexOf(".png") > -1) &&
				(response.results[n].title.indexOf(filename) == -1)) {
				console.log("Removing " + response.results[n].title);
				AP.request({
	                url: '/api/v2/attachments/' + response.results[n].id,
	                type: "DELETE"
				});
				break;
			}
			
			// attachment related to the former MD5
			if((response.results[n].title.indexOf(md5ShortOld + "_") > -1) && (md5Short != md5ShortOld)) {
				console.log("Removing " + response.results[n].title);
				AP.request({
	                url: '/api/v2/attachments/' +  response.results[n].id,
	                type: "DELETE"
				});
				break;
			}
		}
	};
	
	
	// convert the SVG to a base64 encoded PNG
	var convert2PNGBase64 = function(svgS, cCLink, filename, url) {
		
		// get the SVG and the conatiner
		var mySVG = $('.svg-out').find('svg')[0]; 
		var mySVGC = $('.svg-out')[0];
		if(!mySVG) {
			return dfd.resolve();
		}
		
		// does the SVG potentially contain foreignObjects
		var foreignObjectsInvolved = (svgS.toLowerCase().indexOf('foreignobject') > -1) ? true : false;
		
		// check if browser supports PNG generation
		isToDataUrlSupported(foreignObjectsInvolved, function(isSupported) {
			
			// use the browser's native support
			if(isSupported) {
				
				// create a copy of the SVG
				var mySVGCopy = mySVG.cloneNode(true);
				
				// create a canvas a context and an image (in memory)
				var can = document.createElement('canvas'); 
				var ctx = can.getContext('2d');
				var loader = new Image();
				
				// remove the % scaling if present
				if(mySVGCopy.getAttribute('width').indexOf('%') > -1) {
					var viewBoxValues = mySVGCopy.getAttribute('viewBox').split(' ', 4);
					var newWidth = Math.ceil(800 * parseFloat(mySVGCopy.getAttribute('width')) / 100).toString();
					var newHeight = Math.ceil(parseFloat(newWidth) / parseFloat(viewBoxValues[2]) * parseFloat(viewBoxValues[3])).toString();
					mySVGCopy.setAttribute('width', newWidth);
					mySVGCopy.setAttribute('height', newHeight);
				}
				
				// determine raster resolution
				var rasterResolution = parseFloat($(mySVGC).attr("rasterResolution"));
				
				loader.width  = can.width  = Math.ceil(parseFloat(mySVGCopy.getAttribute("width")) * rasterResolution);
				loader.height = can.height = Math.ceil(parseFloat(mySVGCopy.getAttribute("height")) * rasterResolution);
				
				// remove the border if present
				if(mySVGCopy.getAttribute("border") == "true") {
					$(mySVGCopy).css("border", "");
				}
				
				// get the SVG as String
				var svgAsIs = (new XMLSerializer()).serializeToString(mySVGCopy);
				
				// function when SVG is ready
				loader.onload = function() {
					ctx.drawImage(loader, 0, 0, loader.width, loader.height);
					return save(can.toDataURL("image/png"), filename, url);
				};
				
				// function called when an error happens
				loader.onerror = function() {
					console.log("Could not generate image!");
					return dfd.resolve();
				};
				
				// set image source to our SVG
				loader.src = 'data:image/svg+xml,' + encodeURIComponent(svgAsIs);
			}
			
			// use our online conversion service based on phantom.js when browser does not support the above
			else {
				
				// data to post
				var data = {
					width : mySVGC.getAttribute('widthTarget'),
					height : mySVGC.getAttribute('heightTarget'),
					left : mySVGC.getAttribute('leftCrop'),
					right : mySVGC.getAttribute('rightCrop'),
					top : mySVGC.getAttribute('topCrop'),
					bottom : mySVGC.getAttribute('bottomCrop'),
					svg : svgS,
					info : cCLink + " " + filename
				};
				
				// perform post request to the external conversion service
				// remark: the contentType should be application/json. However this type would require a CORS preflight which is not handled by phantom.js
				$.ajax({
					url : "https://svg-out-phantom.herokuapp.com",
					data : JSON.stringify(data),
				    contentType: "text/plain",
					type : "POST",
					success : function(data) {
						return save("," + data, filename, url);
					},
					error : function() {
						console.log('Error receiving PNG!');
						dfd.resolve();
					}
				});
			}
		});
		
		return dfd.promise();
	};
	
		
	// save the base64 encoded PNG
	var save = function(data, filename, url) {
		
		// get the data
		var pngBase64 = data.split(',')[1];
		
		// generate a blob
		var pngBlob = b64toBlob(pngBase64, "image/png", 512);
		
		// generate a file
		var pngFile = blobToFile(pngBlob, filename);
		
		// send the AP request
		AP.request({
            url: url,
            type: "POST",
            data: {file: pngFile},
            contentType: "multipart/form-data",
            success: function (responseText) {
                console.log("Uploaded PNG successfully!");
                return dfd.resolve();
            },
            error: function (request, status, error) {
				console.log("Could not upload PNG!");
				return dfd.resolve();
			}
         });
		
		return dfd.promise();
	};
	
	
	// convert base64 data to a blob
	var b64toBlob = function(b64Data, contentType, sliceSize) {
		contentType = contentType || '';
		sliceSize = sliceSize || 512;
	
		var byteCharacters = atob(b64Data);
		var byteArrays = [];
	
		for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			var slice = byteCharacters.slice(offset, offset + sliceSize);
	
			var byteNumbers = new Array(slice.length);
			for (var i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}
	
			var byteArray = new Uint8Array(byteNumbers);
	
			byteArrays.push(byteArray);
		}
	
		var blob = new Blob(byteArrays, {type: contentType});
		return blob;
	};
	
	
	// convert a blob to a file
	var blobToFile = function(theBlob, fileName) {
	    theBlob.lastModifiedDate = new Date();
	    theBlob.name = fileName;
	    return theBlob;
	};
	
	
	// match a regular expression
	var matcher = function(expression, regex) {
		var match = expression.match(regex);
		if(match) {
			return match[1];
		}
		return null;
	};
	
	
	// check if the browser supports canvas.toDataURL with a SVG being painted on the canvas
	// as Safari supports it but not with foreignObjects involved we check for this conditionally
	var isToDataUrlSupported = function(foreignObjectsInvolved, callback) {
		var can = document.createElement('canvas'); 
		var ctx = can.getContext('2d');
		var loader = new Image();
		
		loader.width  = can.width  = 100;
		loader.height = can.height = 100;
		var svgS = '<svg  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100px" height="100px"><rect x="0" y="0" height="100" width="100" style="stroke:#ff0000; fill: #0000ff"/></svg>';
		var svgSForeignObject = '<svg  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100px" height="100px"><rect x="0" y="0" height="100" width="100" style="stroke:#ff0000; fill: #0000ff"/><foreignObject/></svg>';
		
		loader.onload = function() {
			try {
				ctx.drawImage(loader, 0, 0, loader.width, loader.height);
				can.toDataURL("image/png");
				callback(true);
			}
			catch(err) {
				callback(false);
			}
		};
		loader.onerror = function() {
			callback(false);
		};
		if(foreignObjectsInvolved) {
			loader.src = 'data:image/svg+xml,' + encodeURIComponent(svgSForeignObject);
		}
		else {
			loader.src = 'data:image/svg+xml,' + encodeURIComponent(svgS);
		}
	};
}