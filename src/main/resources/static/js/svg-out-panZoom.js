
// panAndZoom.js for the Confluence svg-out plug-in version 2.1.0
// (C) Andreas Purde 2016
//
// in case you plan to re-use the code please consider the following:
// - the script changes the viewBox => your SVG has to contain a viewBox
// - the script expects that the SVG is embedded in HTML and is already loaded (I did not try other variants)


var svgPanZoom = function(svg, minZoom, maxZoom, useWheel, active) {

	// zoom in at center and apply a pan as well (all coordinates are client coordinates)
	this.panAndZoom = function(relZoom, centerX, centerY, panX, panY) {
	
		// get the bounding rect of the SVG
		var r = svg.getBoundingClientRect();
		
		// calculate the zoom factors and limit the zoom if necessary
		lastZoomFactor = zoomFactor;
		zoomFactor = zoomFactor * relZoom;
		if(zoomFactor < minZoom) {
			zoomFactor = minZoom;
			relZoom = zoomFactor / lastZoomFactor;
		}
		if(zoomFactor > maxZoom) {
			zoomFactor = maxZoom;
			relZoom = zoomFactor / lastZoomFactor;
		}
		
		// calculate the movements in x- and y-direction due to zoom and pan
		var moveX = ((r.left - centerX) * (1/relZoom - 1) - panX) / (r.right - r.left + 1) * width;
		var moveY = ((r.top - centerY) * (1/relZoom - 1) - panY) / (r.bottom - r.top + 1) * height;
		
		// apply the movements to left and right of the viewbox
		left = left + moveX;
		top = top + moveY;
		
		// the zoom factor goes to width and height
		width = width / relZoom;
		height = height / relZoom;
		
		// calculate the new viewBox
		var newViewBox = Math.round(left).toString() + " " +  Math.round(top).toString() + " " +  Math.round(width).toString() + " " +  Math.round(height).toString();
		
		// apply the new viewBox to the SVG
		svg.setAttribute("viewBox", newViewBox);
	}
	
	
	// react on mouse wheel
	this.wheel = function(evt) {
		
		// in case pan&zoom is not active => return
		if((useWheel == 0) || (active == 0)) {
			return;
		}
		
		evt.preventDefault();
					
		// calculate the relative zoom factor out of the wheel event
		var delta = evt.deltaY;
		delta = -0.3 < delta && delta < 0.3 ? delta : (delta > 0 ? 1 : -1) * Math.log(Math.abs(delta) + 10) / 30;
		
		// zoom in at the current position
		this.panAndZoom(1 / Math.pow(1.2, delta), evt.clientX, evt.clientY, 0, 0);
	}
	
	
	// react on mouse move
	this.move = function(evt) {
		evt.preventDefault();
					
		// in case the button is not pressed any more terminate a move
		if(evt.buttons != 1) {
			move_ongoing = 0;
		}
		
		// in case pan&zoom is not active => return
		if(active == 0) {
			return;
		}

		// in case a move with pressed left mouse button is ongoing
		if(move_ongoing == 1) {
			this.panAndZoom(1.0, 0, 0, evt.clientX - lastMoveX, evt.clientY - lastMoveY);
			lastMoveX = evt.clientX;
			lastMoveY = evt.clientY;
		}
	}
	
	
	// react on mouse down (the beginning of a move)
	this.down = function(evt) {
		evt.preventDefault();
					
		// move is going on
		move_ongoing = 1;
		
		// keep the coordinates
		lastMoveX = evt.clientX;
		lastMoveY = evt.clientY;
		firstMoveX = evt.clientX;
		firstMoveY = evt.clientY;
	}
	
	
	// check if the target of the event is a link or a child of a link
	this.isChildOfLink = function(evt) {
		var n = evt.target;
		while(n != null)
		{
			if(n.tagName) {
				if(n.tagName.toLowerCase() == "a") {
					return true;
				}
			}
			n = n.parentNode;
		}
		return false;
	}
	
	
	// click event (fired after mouse down and up)
	this.click = function(evt) {
	
		// prevent the default action only in case we had a bigger move (more than 10 pixel)
		// preventing the default action prevents the execution of a link after a move here
		if(Math.abs(firstMoveX - evt.clientX) > 10 || Math.abs(firstMoveY - evt.clientY) > 10) {
			evt.preventDefault();
		}
		
		// in case it was a real click we may need to activate pan and zoom
		else {
			if(active == 0) {
				// in case the user clicks somewhere which is not a link
				if(this.isChildOfLink(evt) == false) {
					active = 1;
				}
			}
		}
	}
	
	
	// react to touchstart, touchend and touchcancel
	this.touchinit = function(evt) {
		// in case we only have one finger
		if(evt.touches.length == 1) {
			// store the positions of this finger
			firstFingerX = evt.touches[0].clientX;
			firstFingerY = evt.touches[0].clientY;
		}
		// in case we have two fingers
		if(evt.touches.length == 2) {
			// store the position of the second finger
			secondFingerX = evt.touches[1].clientX;
			secondFingerY = evt.touches[1].clientY;
		}
	}
	
	
	// react on touchmove
	this.touchmove = function(evt) {
	
		// in case pan&zoom is not active => return
		if(active == 0) {
			return;
		}
	
		// a move with just one finger = pan
		if(evt.touches.length == 1) {
			evt.preventDefault();
			this.panAndZoom(1.0, 0, 0, evt.touches[0].clientX - firstFingerX, evt.touches[0].clientY - firstFingerY);
			firstFingerX = evt.touches[0].clientX;
			firstFingerY = evt.touches[0].clientY;
		}
		
		// a move with two fingers = combination of zoom an pan
		if(evt.touches.length == 2) {
			evt.preventDefault();
			
			// the relative zoom is the ratio between the distance between the two fingers now and at the last event
			var relZoom = Math.sqrt(Math.pow(evt.touches[1].clientX - evt.touches[0].clientX,2) +
				Math.pow(evt.touches[1].clientY - evt.touches[0].clientY,2)) /
				Math.sqrt(Math.pow(secondFingerX - firstFingerX,2) +
				Math.pow(secondFingerY - firstFingerY, 2));
			
			// the center of zoom is the middle point of the two fingers
			var centerX = (evt.touches[1].clientX + evt.touches[0].clientX) / 2;
			var centerY = (evt.touches[1].clientY + evt.touches[0].clientY) / 2;
			
			// the pan part is the difference between the two centers now and at at the last event
			var moveX = centerX - (secondFingerX + firstFingerX) / 2;
			var moveY = centerY - (secondFingerY + firstFingerY) / 2;
			
			// pan and zoom what we have calculated
			this.panAndZoom(relZoom, centerX, centerY, moveX, moveY);
			
			// save the positions
			firstFingerX = evt.touches[0].clientX;
			firstFingerY = evt.touches[0].clientY;
			secondFingerX = evt.touches[1].clientX;
			secondFingerY = evt.touches[1].clientY;
		}
	}
	
	
	// split the viewBox into its parts
	this.splitViewbox = function() {
		var viewBoxValues = svg.getAttribute("viewBox").split(' ', 4);
		left = parseFloat(viewBoxValues[0]);
		top = parseFloat(viewBoxValues[1]);
		width = parseFloat(viewBoxValues[2]);
		height = parseFloat(viewBoxValues[3]);
	}
	
	
	// zoom in when the corresponding button is pressed (zoom center = center of image)
	this.zoomIn = function(evt) {
		var r = svg.getBoundingClientRect();
		this.panAndZoom(1.2, (r.right + r.left) / 2, (r.bottom + r.top) / 2 , 0, 0);
		active = 1;
	}
	
	
	// zoom out when the corresponding button is pressed (zoom center = center of image)
	this.zoomOut = function(evt) {
		var r = svg.getBoundingClientRect();
		this.panAndZoom(1 / 1.2, (r.right + r.left) / 2, (r.bottom + r.top) / 2, 0, 0);
		active = 1;
	}
	
	
	// reset all zoom and pan activities
	this.reset = function(evt) {
		zoomFactor = 1.0;
		lastZoomFactor = 1.0;
		svg.setAttribute("viewBox", originalViewBox);
		this.splitViewbox();
	}

	
	// init variables
	var move_ongoing = 0;
	var lastMoveX = 0;
	var lastMoveY = 0;
	var firstMoveX = 0;
	var firstMoveY = 0;
	var zoomFactor = 1.0;
	var lastZoomFactor = 1.0;
	var left = 0;
	var top = 0;
	var width = 0;
	var height = 0;
	var originalViewBox = svg.getAttribute("viewBox");
	this.splitViewbox();
	
	// additional variables for the touch events
	var firstFingerX = 0;
	var firstFingerY = 0;
	var secondFingerX = 0;
	var secondFingerY = 0;
	
	// add event listeners
	svg.addEventListener("wheel", this.wheel.bind(this), {passive: false});
	svg.addEventListener("mousemove", this.move.bind(this), false);
	svg.addEventListener("mousedown", this.down.bind(this), false);
	svg.addEventListener("click", this.click.bind(this), false);
	
	// add event listeners for touch-events
	svg.addEventListener("touchstart", this.touchinit.bind(this), false);
	svg.addEventListener("touchend", this.touchinit.bind(this), false);
	svg.addEventListener("touchmove", this.touchmove.bind(this), false);
	svg.addEventListener("touchcancel", this.touchinit.bind(this), false);
}
	
	
function panZoom() {
	
	// get all svg-outs on the page
	var svgouts = document.getElementsByClassName("svg-out");
		
	// the row of buttons (zoom in, reset, zoom out) with place holder for the function to be called when clicked
	var buttonRow = "<button id=\"$zoomInID\" class=\"aui-button\" style=\"width:30px;height:30px;margin-right:1px\">+</button> <button id=\"$zoomResetID\" class=\"aui-button\" style=\"width:30px;height:30px;margin-left:1px;margin-right:1px\">&#8634</button> <button id=\"$zoomOutID\" class=\"aui-button\" style=\"width:30px;height:30px;margin-left:1px\">&#x2212</button>";
		
	// loop through all svg-outs
	for(s = 0; s < svgouts.length; s++) {
	
		// in case the panAndZoom attribute is true
		if(svgouts[s].getAttribute("panAndZoom") == "true") {
			
			// enable pan and zoom for that svg-out
			var startActive = (svgouts[s].getAttribute("startActive") == "false") ? 0 : 1;
			var useWheel = (svgouts[s].getAttribute("useWheel") == "false") ? 0 : 1;
			var pz = new svgPanZoom(svgouts[s].firstElementChild, parseFloat(svgouts[s].getAttribute("minZoom")), parseFloat(svgouts[s].getAttribute("maxZoom")), useWheel, startActive);
			
			// buttons wanted?
			if(svgouts[s].getAttribute("zoomButtons") == "true") {
			
				// create a <p> for the buttons
				var buttonP = document.createElement("p");
				var currentID = "svg-out-button-" + s.toString() + "-";
				
				// fill the <p> with our three buttons
				buttonP.innerHTML = buttonRow.replace("$zoomInID", currentID + "zoomIn").replace("$zoomOutID",
					currentID + "zoomOut").replace("$zoomResetID", currentID + "zoomReset");
					
				// append the <p>...</p> to our svg-out span
				svgouts[s].appendChild(buttonP);
				
				// add the event listeners to the buttons
				document.getElementById("svg-out-button-" + s.toString() + "-zoomIn").addEventListener("click", pz.zoomIn.bind(pz));
				document.getElementById("svg-out-button-" + s.toString() + "-zoomOut").addEventListener("click", pz.zoomOut.bind(pz));
				document.getElementById("svg-out-button-" + s.toString() + "-zoomReset").addEventListener("click", pz.reset.bind(pz));
			}
		}
	}
};
