$('#loadMessage').delay(1500).show(0);
 
// load the SVG and insert it into the page
var baseUrl = $(".svg-out").data("base-url");
var currentPageId = $(".svg-out").attr("pageId");
var isInline = $(".svg-out").data("is-inline");
var outputType = $(".svg-out").data("output-type");
var loader = new svgLoader();
loader.loadAndInsertSVG(loadFinished);
		
// prepare a converter
var converter = new svg2PNGConverter();

// callback when loading is finished
function loadFinished(result) {
				
	// success
	if(result == "ok") {
		panZoom();
		replaceLinks(baseUrl, true);
		
		if(isInline) {
			var boxWidth = Math.ceil(document.getElementById("svg-content").getBoundingClientRect().width) + 1.0;
			var boxHeight = Math.ceil(document.getElementById("svg-content").getBoundingClientRect().height) + 1.0;
			AP.resize(boxWidth.toString() + "px", boxHeight.toString() + "px");
		}
		
		if(outputType !== "preview") {
			converter.checkIfConversionNeededFromDOM(currentPageId, function(isNeeded, filename, url) {
			
				// conversion needed?
				if(isNeeded) {
					
					// show a working flag
					workingFlag = AP.flag.create({
					    type: 'info',
					    title: $("#genPNGTitle").text(),
					    body: $("#genPNG").text()
					});
					
					// convert
					converter.convert(loader.getSVG(), baseUrl, filename, url).then(function(){
						workingFlag.close();
					});
				}
			});
		}				
	}
	
	// could not load attachment
	else if(result == "could_not_load") {
		document.getElementsByClassName("svg-out")[0].innerHTML = $("#errorFind").text();
	}
	
	// negative security check
	else {
		var message = $("#securityCheck").text();
		var linkToConfig = baseUrl + "/plugins/servlet/ac/de.edrup.confluence.plugins.svg-out/configure";
		document.getElementsByClassName("svg-out")[0].innerHTML = message.replace("$path", linkToConfig) + " " + result;
	}
}