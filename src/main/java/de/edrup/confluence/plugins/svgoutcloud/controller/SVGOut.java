package de.edrup.confluence.plugins.svgoutcloud.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.URLDecoder;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.ContextJwt;
import com.coverity.security.Escape;

import de.edrup.confluence.plugins.svgoutcloud.util.SafeSettings;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class SVGOut {
	
	@Autowired
	private AtlassianHostRestClients atlassianHostRestClients;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private RestTemplateBuilder restTemplate;
	
	@Autowired
	private SafeSettings safeSettings;
	
	private static final Logger log = LoggerFactory.getLogger(SVGOut.class);
	
	// svg-out macro output
	@RequestMapping(value = "/svg-out", method = RequestMethod.GET)
	public ModelAndView svgOutMacro(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="pageID") String pageID,
		@RequestParam(value="attachmentID") String attachmentID,
		@RequestParam(value="height", defaultValue="undefined") String height,
		@RequestParam(value="width", defaultValue="undefined") String width,
		@RequestParam(value="align", defaultValue="0") String align,
		@RequestParam(value="top", defaultValue="0%") String top,
		@RequestParam(value="bottom", defaultValue="100%") String bottom,
		@RequestParam(value="left", defaultValue="0%") String left,
		@RequestParam(value="right", defaultValue="100%") String right,
		@RequestParam(value="panZoom", defaultValue="false") String panZoom,
		@RequestParam(value="startActive", defaultValue="true") String startActive,
		@RequestParam(value="useWheel", defaultValue="true") String useWheel,
		@RequestParam(value="zoomButtons", defaultValue="false") String zoomButtons,
		@RequestParam(value="minZoom", defaultValue="0.1") String minZoom,
		@RequestParam(value="maxZoom", defaultValue="10") String maxZoom,
		@RequestParam(value="border", defaultValue="false") String border,
		@RequestParam(value="linkList", defaultValue="[]") String linkList,
		@RequestParam(value="exportPNG", defaultValue="false") String exportPNG,
		@RequestParam(value="md5Short", defaultValue="0") String md5Short,
		@RequestParam(value="lic", defaultValue="") String lic,
		@RequestParam(value="inline", defaultValue="false") String inline,
		@RequestParam(value="svgUrl", defaultValue="") String svgUrl,
		@RequestParam(value="rasterResolution", defaultValue="1.0") String rasterResolution,
		@RequestParam(value="_page", defaultValue="") String _page,
		@RequestParam(value="_name", defaultValue="") String _name,
		@RequestParam(value="_scaleX", defaultValue="") String _scaleX,
		@RequestParam(value="_scaleY", defaultValue="") String _scaleY,
		@RequestParam(value="_panAndZoom", defaultValue="") String _panAndZoom,
		@RequestParam(value="linkHighlightHover", defaultValue="true") String linkHighlightHover,
		@RequestParam(value="linkHighlight", defaultValue="false") String linkHighlight,
		@RequestParam(value="outputType", defaultValue="") String outputType,
		@RequestParam(value="allowUnsafe", defaultValue="false") boolean allowUnsafe,
		@RequestParam(value="v2above", defaultValue="false") boolean v2above,
		HttpServletResponse response) {
		
		if(attachmentID.isEmpty() && _name.isEmpty() && svgUrl.isEmpty()) {
			ModelAndView model = new ModelAndView("svg-out-frame-empty");
			return model;
		}
		
		if(!lic.equals("active") && !hostUser.getHost().getBaseUrl().contains("edrup-dev")) {
			ModelAndView model = new ModelAndView("svg-out-frame-license");
			return model;
		}
		
		if(!v2above) {
			ModelAndView model = new ModelAndView("svg-out-frame-v2above");
			return model;
		}
		
		System.out.printf("INFO:  %s: processing attachment %s%n", hostUser.getHost().getBaseUrl(), attachmentID);
		
		// Server/DC migration
		if(panZoom.equals("false") && _panAndZoom.equals("true")) {
			panZoom = "true";
		}
		if(height.equals("undefined") && !_scaleY.isEmpty()) {
			height = _scaleY;
		}
		if(width.equals("undefined") && !_scaleX.isEmpty()) {
			width = _scaleX;
		}
		String alignTemp = align;
		switch(alignTemp) {
			case "left":
				align = "0";
				break;
			case "center":
				align = "1";
				break;
			case "right":
				align = "2";
				break;
			default:
				break;
		}
		
		ModelAndView model = new ModelAndView("svg-out-frame");
		model.addObject("baseUrl", getDisplayUrlSafe(hostUser.getHost()));
		model.addObject("currentPageId", pageID);
		model.addObject("isInline", "true".equals(inline));
		model.addObject("align", align);
		model.addObject("width", width);
		model.addObject("height", height);
		model.addObject("left", left);
		model.addObject("right", right);
		model.addObject("top", top);
		model.addObject("bottom", bottom);
		model.addObject("border", border);
		model.addObject("panZoom", panZoom);
		model.addObject("useWheel", useWheel);
		model.addObject("startActive", startActive);
		model.addObject("zoomButtons", zoomButtons);
		model.addObject("minZoom", minZoom);
		model.addObject("maxZoom", maxZoom);
		model.addObject("attachmentID", attachmentID);
		model.addObject("linkList", linkList);
		model.addObject("exportPNG", exportPNG);
		model.addObject("md5Short", md5Short);
		model.addObject("pageId", pageID);
		model.addObject("svgUrl", svgUrl);
		model.addObject("rasterResolution", rasterResolution);
		model.addObject("linkHighlight", linkHighlight);
		model.addObject("linkHighlightHover", linkHighlightHover);
		model.addObject("outputType", outputType);
		model.addObject("allowUnsafe", allowUnsafe);
		
		// inline script handling
		String noncePool = createNoncePool();
		model.addObject("noncePool", noncePool);
		response.setHeader("Content-Security-Policy", buildCSP(noncePool, allowUnsafe));
			
		// Server/DC migration
		model.addObject("_page", _page.replaceFirst(".*title=", "").replaceFirst(",.*", ""));
		model.addObject("_name", _name.replaceFirst(".*filename=", "").replaceFirst(",.*", ""));
		
		return model;
	}
	
	
	// svg-out editor
	@RequestMapping(value = "/svg-editor", method = RequestMethod.GET)
	public ModelAndView svgOutEditor(@AuthenticationPrincipal AtlassianHostUser hostUser, HttpServletResponse response) {
				
		// prepare the model
	    ModelAndView model = new ModelAndView("svg-out-editor");
	    model.addObject("baseUrl", getDisplayUrlSafe(hostUser.getHost()));
	    
		// inline script handling
		String noncePool = createNoncePool();
		model.addObject("noncePool", noncePool);
		response.setHeader("Content-Security-Policy", buildCSP(noncePool, false));
	    
	    // and return it
		return model;
	}
	
	
	// svg-out-cloud configuration
	@RequestMapping(value = "/configure", method = RequestMethod.GET)
	public ModelAndView svgOutConfigure(@AuthenticationPrincipal AtlassianHostUser hostUser) {
		
	    ModelAndView model = new ModelAndView("svg-out-configure");
	 
	    return model;
	}
	
	
	// svg-out macro output in static mode (e.g. PDF)
	@RequestMapping(value = "/svg-out-static", method = RequestMethod.GET)
	@ResponseBody
	public String svgOutMacroStatic(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="pageID") String pageID,
		@RequestParam(value="attachmentID") String attachmentID,
		@RequestParam(value="align", defaultValue="0") String align,
		@RequestParam(value="exportPNG", defaultValue="false") String exportPNG,
		@RequestParam(value="md5Short", defaultValue="") String md5Short,
		@RequestParam(value="width", defaultValue="undefined") String width,
		@RequestParam(value="border", defaultValue="false") String border,
		@RequestParam(value="percentSupported", defaultValue="false") String percentSupported,
		@RequestParam(value="inline", defaultValue="false") String inline) {
		
		// build the static output
		String svgOutStatic = "";
				
		if(exportPNG.equals("true")) {
			
			JSONObject content = new JSONObject(atlassianHostRestClients.authenticatedAsHostActor().getForObject(String.format("%s/api/v2/attachments/%s", hostUser.getHost().getBaseUrl(), attachmentID), String.class));
			String download = content.getJSONObject("_links").getString("download");
			
			// build the path to the PNG
			String version = myMatcher(download, "(?:version=)(.*?)(?:(&|$))");
			String svgFileName = myMatcher(download, "(?i)(?:attachments/.*?/)(.*?)(?:.svgz?(\\?|$))");
			try {
				svgFileName = URLDecoder.decode(svgFileName, "UTF-8");
			}
			catch(Exception e) {}
			svgFileName = svgFileName.replaceAll("[^a-zA-Z0-9]", "");
			String path2PNG = getDisplayUrlSafe(hostUser) + "/download/attachments/" + pageID + "/" + md5Short + "_" + version + "_" + svgFileName + ".png";
						
			System.out.printf("INFO:  %s: processing attachment for printing %s%n", hostUser.getHost().getBaseUrl(), path2PNG);

			String styleProp = "";
			switch(align) {
				case "0": styleProp = "style='text-align:left'"; break;
				case "1": styleProp = "style='text-align:center'"; break;
				case "2": styleProp = "style='text-align:right'"; break;
				default: break;
			}
			
			String imgStyleProp = "style='";
			if(border.equals("true")) {
				imgStyleProp += "border:1px solid black;";
			}
			if(width.contains("px")) {
				imgStyleProp += "width:" + width + ";";
			}
			imgStyleProp += "'";
			
			String widthProp = "";
			if(width.contains("%%") && percentSupported.equals("true")) {
				widthProp = " width='" + Escape.html(width.replace("%%", "%")) + "'";
			}
			
			svgOutStatic = "false".equals(inline) ? "<div " + styleProp + widthProp + "><img class='confluence-embedded-image' src='" + path2PNG + "' "+ imgStyleProp + "></img></div>" : "<img class='confluence-embedded-image' src='" + path2PNG + "' "+ imgStyleProp + "></img>";
			System.out.println(svgOutStatic);
		}
		else {
			svgOutStatic = messageSource.getMessage("svg-out-cloud.message.exportNotSelected", null, Locale.getDefault());
		}
				
		return svgOutStatic;
	}
	
	
	// load compressed SVGZ and return it decompressed
	@ContextJwt
	@RequestMapping(value = "/decompress", method = RequestMethod.GET)
	public ResponseEntity<String> svgOutDecompress(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="download") String download) {
						
		System.out.printf("INFO: %s: decompressing attachment %s%n", hostUser.getHost().getBaseUrl(), download);
		
		// prepare the template for binary download
		atlassianHostRestClients.authenticatedAsHostActor().getMessageConverters().add(new ByteArrayHttpMessageConverter());    
				
		String svgS = loadSVG(hostUser.getHost().getBaseUrl(), download);
		
		// prepare the response
		HttpHeaders rHeaders= new HttpHeaders();
		if((svgS.length() > 0) && download.contains("?version=")) {
			rHeaders.setCacheControl("private, max-age=2592000");
		}
		ResponseEntity<String> response = new ResponseEntity<String>(svgS, rHeaders, HttpStatus.OK);
		
		return response;
	}
	
	
	// load external SVG
	@ContextJwt
	@RequestMapping(value = "/external", method = RequestMethod.GET)
	public ResponseEntity<String> svgOutExternal(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="download") String download, HttpServletResponse sResponse) {
		
		String svgS = loadExternalSVG(download);
		sResponse.setHeader("Content-Disposition", "attachment");
		return new ResponseEntity<String>(svgS, svgS.length() > 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
	}
	
	
	// determine the content checksum of an external SVG
	@ContextJwt
	@RequestMapping(value = "/externalhash", method = RequestMethod.GET)
	public ResponseEntity<String> svgOutExternalHash(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="download") String download) {
		
		String svgS = loadExternalSVG(download);
		ResponseEntity<String> response = new ResponseEntity<String>(Integer.toString(svgS.hashCode(), 32), svgS.length() > 0 ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
		
		return response;
	}
	
	
	// load an SVG 
	private String loadSVG(String baseUrl, String download) {
		
		String svgS = "";
		
		String url ="";
		try {
			url = URLDecoder.decode(baseUrl + download, "UTF-8");
		}
		catch(Exception e) {
			System.out.printf("ERROR: %s: for the attachment %s the url could not be generated: %s%n", baseUrl, download, e.toString());
			return "";
		}
		
		// download compressed SVG
		if(url.toLowerCase().contains(".svgz")) {
			
			// prepare the template for binary download
			atlassianHostRestClients.authenticatedAsHostActor().getMessageConverters().add(new ByteArrayHttpMessageConverter());    
					
			try {
				// download the compressed SVGZ
				ResponseEntity<byte[]> response = atlassianHostRestClients.authenticatedAsHostActor().getForEntity(url, byte[].class);
				
				// decompress it
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(response.getBody())), out);
				svgS = out.toString("UTF-8");
			}
			catch(Exception e) {
				System.out.printf("ERROR: %s: the attachment %s could not be loaded or decompressed: %s%n", baseUrl, download, e.toString());
			}
		}
		
		// download normal SVG
		else {
			try {
				svgS = atlassianHostRestClients.authenticatedAsHostActor().getForObject(url, String.class);
			}
			catch(Exception e) {
				System.out.printf("ERROR: %s: the attachment %s could not be loaded: %s%n", baseUrl, download, e.toString());
			}
		}
		
		return svgS;
	}
	
	
	// load an external SVG from the web
	private String loadExternalSVG(String download) {
		String svgS = "";
		
		try {
			ResponseEntity<byte[]> response = restTemplate.build().exchange(download, HttpMethod.GET, null, byte[].class);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			if(download.toLowerCase().contains(".svgz")) {
				IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(response.getBody())), out);
				svgS = out.toString("UTF-8");
			}
			else {
				svgS = new String(response.getBody(), "UTF-8");
			}
		}
		catch(Exception e) {
			log.error("Could not download {}: {}", download, e.toString());
		}
		
		return svgS;
	}
	
	
	// extract a match
	private String myMatcher(String expression, String regex) {
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(expression);
		if(m.find()) {
			return m.group(1);
		}
		return "";
	}
	
	
	// get the settings
	@ContextJwt
	@RequestMapping(value = "/settings", method = RequestMethod.GET)
	public ResponseEntity<String> getSettings(@AuthenticationPrincipal AtlassianHostUser hostUser) {
		return new ResponseEntity<String>(safeSettings.getSettings(hostUser), HttpStatus.OK);
	}
	
	
	// set the settings
	@ContextJwt
	@RequestMapping(value = "/settings", method = RequestMethod.PUT)
	public ResponseEntity<String> setSettings(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody Map<String, Object> payload) {
		if(safeSettings.isAdmin(hostUser)) {
			JSONObject settings = new JSONObject(payload);
			return safeSettings.setSettings(hostUser, settings);
		}
		else {
			return new ResponseEntity<String>("", HttpStatus.FORBIDDEN);
		}
	}
	
	
	// build CSP
	private String buildCSP(String noncePool, boolean allowUnsafe) {
		return allowUnsafe ? "script-src 'self' 'unsafe-inline' 'unsafe-eval' https://connect-cdn.atl-paas.net https://cdnjs.cloudflare.com; object-src 'none'" :
			"script-src 'self' https://connect-cdn.atl-paas.net https://cdnjs.cloudflare.com " + noncePool + "; object-src 'none'";
	}
	
	
	// create some nonces
	private String createNoncePool() {
		String pool = "";
		for(int n = 0; n < 10; n++) {
			pool += "'nonce-" + UUID.randomUUID().toString() + "' ";
		}
		return pool.trim();
	}
	
	
	// get the displayUrl in a safe way
	private String getDisplayUrlSafe(AtlassianHostUser hostUser) {
		return getDisplayUrlSafe(hostUser.getHost());
	}
	
	
	// get the displayUrl in a safe way
	private String getDisplayUrlSafe(AtlassianHost host) {
		return host.getDisplayUrl() != null ? host.getDisplayUrl() : host.getBaseUrl();
	}
}
