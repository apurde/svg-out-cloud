package de.edrup.confluence.plugins.svgoutcloud.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class SafeSettings {
	
	@Value("${SALT}")
    private String salt;
	
	@Autowired
	private AtlassianHostRestClients atlassianHostRestClients;
	
	static final Logger log = LoggerFactory.getLogger(SafeSettings.class);

	
	public String getSettings(AtlassianHostUser hostUser) {
		JSONObject settings = getJSONObjectAsUser(hostUser.getHost().getBaseUrl() + "/rest/atlassian-connect/1/addons/de.edrup.confluence.plugins.svg-out/properties/svg-out-settings-2?jsonValue=true");
		if(areSettingsValid(hostUser, settings)) {
			log.debug(settings.toString());
			return settings.toString();
		}
		else {
			if(!settings.isEmpty()) log.warn("Settings {} of host {} are not valid", settings.toString(), hostUser.getHost().getBaseUrl());
			return getDefaultSettings().toString();
		}
	}
	
	
	public ResponseEntity<String> setSettings(AtlassianHostUser hostUser, JSONObject settings) {
		settings.put("hash", calculateIndividualSettingsHash(hostUser.getHost(), settings));
		return putJSONAsUser(hostUser.getHost().getBaseUrl() + "/rest/atlassian-connect/1/addons/de.edrup.confluence.plugins.svg-out/properties/svg-out-settings-2?jsonValue=true", settings);
	}
	
	
	public boolean isAdmin(AtlassianHostUser hostUser) {
		try {
			JSONArray operations = getJSONObjectAsUser(hostUser.getHost().getBaseUrl() + "/rest/api/user/current?expand=operations").getJSONArray("operations");
			for(int n = 0; n < operations.length(); n++) {
				if(operations.getJSONObject(n).getString("operation").equals("administer") && operations.getJSONObject(n).getString("targetType").equals("application")) {
					return true;
				}
			}
		}
		catch(Exception e) {
			log.error(e.toString());
		}
		return false;
	}
	
	
	private JSONObject getJSONObjectAsUser(String url) {
		try {
			return new JSONObject(atlassianHostRestClients.authenticatedAsHostActor().getForObject(url, String.class));
		}
		catch(Exception e) {
			log.error("Could not get {} as user: {}", url, e.toString());
			return new JSONObject();			
		}
	}
	
	
	private ResponseEntity<String> putJSONAsUser(String url, JSONObject payload) {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(payload.toString(), headers);
			return atlassianHostRestClients.authenticatedAsHostActor().exchange(url, HttpMethod.PUT, entity, String.class);
		}
		catch(Exception e) {
			log.error("Could not put as app {}: {}", url, e.toString());
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	private JSONObject getDefaultSettings() {
		JSONObject value = new JSONObject();
		value.put("securityMode", "internal").put("whiteList", "").put("exportPNGDefault", "false");
		return new JSONObject().put("value", value);
	}
	
	
	private boolean areSettingsValid(AtlassianHostUser hostUser, JSONObject settings) {
		if(settings.has("value")) {
			JSONObject settingsClone = new JSONObject(settings.toString()).getJSONObject("value");
			if(calculateIndividualSettingsHash(hostUser.getHost(), settingsClone).equals(settingsClone.optString("hash"))) {
				return true;
			}
			else {
				if(calculateLegacySettingsHash(settingsClone).equals(settingsClone.optString("hash"))) {
					setSettings(hostUser, settingsClone);
					log.info("Converted settings for host {}", hostUser.getHost().getBaseUrl());
					return true;
				}
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	
	private String calculateLegacySettingsHash(JSONObject settings) {
		String stringToHash = settings.optString("securityCheck") + settings.optString("whiteList") + salt;
		log.debug("String to hash: {}", stringToHash);
		return SHA256Calc(stringToHash);
	}
	
	
	private String calculateIndividualSettingsHash(AtlassianHost host, JSONObject settings) {
		String stringToHash = settings.optString("securityCheck") + settings.optString("whiteList") + host.getClientKey() + salt;
		log.debug("String to hash: {}", stringToHash);
		return SHA256Calc(stringToHash);		
	}
	
	
	private String SHA256Calc(String value) {
		byte[] bytesOfValue;
		try {
			bytesOfValue = value.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			return e.toString();
		}
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[] sha = md.digest(bytesOfValue);
			return bytesToHex(sha);
		} catch (NoSuchAlgorithmException e) {
			return e.toString();
		}
	}
	
	
	private String bytesToHex(byte[] in) {
	    final StringBuilder builder = new StringBuilder();
	    for(byte b : in) {
	        builder.append(String.format("%02x", b));
	    }
	    return builder.toString();
	}
}
