package de.edrup.confluence.plugins.svgoutcloud.controller;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

@WebFilter("/*")
@Component
public class SVGOutFilter implements Filter {  
	
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        httpServletResponse.setHeader("Strict-Transport-Security", "max-age=31536000");
        httpServletResponse.setHeader("Content-Security-Policy", "script-src 'self' https://connect-cdn.atl-paas.net https://cdnjs.cloudflare.com; object-src 'none'");
        httpServletResponse.setHeader("Referrer-Policy", "strict-origin-when-cross-origin");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
